<?php
session_start();
ob_start();
ini_set('session_auto_start', 'on');
ini_set('display_errors', '1');
require_once('include/vue_generique.php');
require_once('include/controleur_generique.php');
require_once('include/modele_generique.php');
require_once('include/module_generique.php');
require_once("include/modele_generique_excepetion.php");


$modGen = new ModeleGenerique();
$modGen::init();

if(isset($_GET['module'])){
    $nom_module = $_GET['module'];
}else{
    $nom_module = "accueil";
}


switch ($nom_module) {
    case 'Accueil':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_accueil();
        }
        break;
    case 'Gallery':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_Gallery();
        }
        break;
    case 'ajout_Gallery':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_formulaire_ajout_gallery();
        }
        break;
    case 'ajout_video_Gallery':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_formulaire_ajout_video_gallery();
        }
        break;
    case 'ajout_Photo':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        var_dump($_POST);
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_ajout_gallery($_POST);
        }
        break;
    case 'ajout_Video':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_ajout_video_gallery($_POST);
        }
        break;
    case 'modif_video':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_modif_video_gallery($_POST);
        }
        break;
    case 'modif_photo':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_Gallery($_GET['err']);
        }else{
            $module->module_modif_gallery($_POST);
        }
        break;
    case 'modif_Acueil':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_forumaire_modif_accueil();
        }
        break;
    case 'modif_post_Acueil':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_modif_post_accueil($_POST);
        }
        break;
    case 'service':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->module_service();
        }
        break;
    case 'detail_service':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->module_post_service();
        }
        break;
    case 'ajout_Photo_post':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
         
        if(isset($_GET['err'])){

        }else{
            $module->module_ajout_photo_post($_POST);
        }
        break;
    case 'modif_photo_existant':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->modif_photo_existant($_POST);
        }
        break;
    case 'form_ajout_post_service':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->form_ajout_post_service();
        }
        break;

    case 'modif_post':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
       
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_modif_post($_POST);
        }
        break;
    case 'supprimer_post':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_suppr_post($_GET);
        }
        break;
    case 'ajout_post':
        include_once('module/mod_service/mod_service.php');
        $nom_classe_module = "modservice";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_ajout_post($_POST);
        }
        break;
    case 'propos':
        include_once('module/mod_propos/mod_propos.php');
        $nom_classe_module = "modpropos";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_propos();
        }
        break;
        case 'form_modif_post_propos':
        include_once('module/mod_propos/mod_propos.php');
        $nom_classe_module = "modpropos";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
            $module->module_err_accueil($_GET['err']);
        }else{
            $module->module_forumaire_modif_propos();
        }
        break;
	case 'formulaire_equipe':
		include_once('module/mod_equipe/mod_equipe.php');
		$nom_classe_module = "modequipe";
		$module = new $nom_classe_module();
		if(isset($_GET['err'])){

		}else{
			$module->module_formulaire_equipe();
		}
		break;
	case 'modif_personne':
		include_once('module/mod_equipe/mod_equipe.php');
		$nom_classe_module = "modequipe";
		$module = new $nom_classe_module();
		if(isset($_GET['err'])){

		}else{
			$module->modif_equipe();
		}
		break;
	case 'supprimer_equipe':
		include_once('module/mod_equipe/mod_equipe.php');
		$nom_classe_module = "modequipe";
		$module = new $nom_classe_module();
		if(isset($_GET['err'])){

		}else{
			$module->supprimer_equipe();
		}
		break;
	case 'ajouter_equipe':
		include_once('module/mod_equipe/mod_equipe.php');
		$nom_classe_module = "modequipe";
		$module = new $nom_classe_module();
		if(isset($_GET['err'])){

		}else{
			$module->ajouter_equipe();
		}
		break;
    case 'modif_photo_personne':
        include_once('module/mod_equipe/mod_equipe.php');
        $nom_classe_module = "modequipe";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->modif_photo_personne();
        }
        break;

    case 'contact':
        include_once('module/mod_contact/mod_contact.php');
        $nom_classe_module = "modcontact";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->module_contact();
        }

        break;
    case 'ajouter_tag':
        include_once('module/mod_tag/mod_tag.php');
        $nom_classe_module = "moduletag";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->affiche_formulaire_Tag();
        }

        break;

    case 'ajout_Tag':
        include_once('module/mod_tag/mod_tag.php');
        $nom_classe_module = "moduletag";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->ajouter_Tag();
        }
        break;
    case'ajouterTagPhoto':
        include_once('module/mod_tag/mod_tag.php');
        $nom_classe_module = "moduletag";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->ajouter_Tag_Photo();
        }
        break;
    case 'admin':
        include_once('module/mod_Connexion/mod_connexion.php');
        $nom_classe_module = "ModConnexion";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->formulaireConnexion();
        }
        break;
        
    case 'connexion':
        include_once('module/mod_Connexion/mod_connexion.php');
        $nom_classe_module = "ModConnexion";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->connexion();
        }
        break;
    case 'form_mdp':
        include_once('module/mod_Connexion/mod_connexion.php');
        $nom_classe_module = "ModConnexion";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->form_mdp();
        }
        break;
    case 'change_mdp':
        include_once('module/mod_Connexion/mod_connexion.php');
        $nom_classe_module = "ModConnexion";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->change_mdp();
        }
        break;    
    case 'deconnexion':
        include_once('module/mod_Connexion/mod_connexion.php');
        $nom_classe_module = "ModConnexion";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->deconnexion();
        }
        break;
    case 'admin_photo':
        include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "ModGallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->photo_admin();
        }
        break;
    case 'admin_video':
include_once('module/mod_gallery/mod_gallery.php');
        $nom_classe_module = "modgallery";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->video_admin();
        }
        break;

    case 'form_supprimer_tag':
        include_once('module/mod_tag/mod_tag.php');
        $nom_classe_module = "moduletag";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->affiche_formulaire_supprimer_Tag();
        }

    break;
    case 'supr_Tag':
        include_once('module/mod_tag/mod_tag.php');
        $nom_classe_module = "moduletag";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){
        }else{
            $module->supr_Tag();
        }
    break;
    case 'legal':
        include_once('module/mod_accueil/mod_accueil.php');
        $nom_classe_module = "modaccueil";
        $module = new $nom_classe_module();
        if(isset($_GET['err'])){

        }else{
            $module->mention();
        }
    break;
    default:
        header('Location:index.php?module=Accueil');

}
require_once('include/template.php');
