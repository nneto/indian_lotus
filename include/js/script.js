$(document).ready(function () {
    var contentPlacement = $('#container-navbar').height();
    $('.spacer').css('height', contentPlacement);
});

$(document).scroll(function () {
    var row = $('#container-navbar'),
        scrollTop = $(this).scrollTop();
    if (scrollTop > 1) {
        row.addClass('floating-nav');
    } else if (scrollTop <= 1) {
        row.css({
            "box-shadow": "none"
        });
        row.css({
            "opacity": "1"
        });
    }
});


function openNav() {
    document.getElementById("nav-mobile").style.height = "100%";
    document.getElementsByTagName("body")[0].style = "overflow-y: hidden";
}

function closeNav() {
    document.getElementById("nav-mobile").style.height = "0%";
    document.getElementsByTagName("body")[0].style = "overflow-y: auto";
}

//        var $parallaxElement = $('.parallax-bg');
//        var elementHeight = $parallaxElement.outerHeight();
//
//        function parallax() {
//
//            var scrollPos = $(window).scrollTop();
//            var transformValue = scrollPos / 40;
//            var opacityValue = 1 - (scrollPos / 2000);
//            var blurValue = Math.min(scrollPos / 100, 3);
//
//            if (scrollPos < elementHeight) {
//
//                $parallaxElement.css({
//                    'transform': 'translate3d(0, -' + transformValue + '%, 0)',
//                    'opacity': opacityValue,
//                    '-webkit-filter': 'blur(' + blurValue + 'px)'
//                });
//
//            }
//
//        }
//        $(window).scroll(function() {
//            parallax();
//        });

$(document).ready(function () {

    // display section 1 title in sidebar


    // smooth scrolling
    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });

    $(".vertical-nav").on("activate.bs.scrollspy", function () {
        var currentItem = $(".vertical-nav li.active > a").text();
        $('#label span').empty().html(currentItem);
    })
    //              $(document).ready(function() {
    //                $('#lightSlider').lightSlider({
    //                    autoWidth:true,
    //                    loop:true,
    //                    onSliderLoad: function() {
    //                        $('#autoWidth').removeClass('cS-hidden');
    //                    } 
    //                });  
    //              });

    $('.slider').bxSlider({
        mode: 'horizontal',
        captions: false,
        speed: 2000,
        touchEnabled: true,
        autoControls: true,

    });
    var filter = "all",
        itemsNumber = 12,
        $grid;
    get_gallery_items(true);

    $(".filter").click(function () {
        $(".grid").empty();
        filter = $(this).attr("data-filter");
        get_gallery_items(false);
    });

    var lightbox = GLightbox();
    var lightboxDescription = GLightbox({
        selector: 'glightbox2'
    });
    var lightboxVideo = GLightbox({
        selector: 'glightbox3',
        jwplayer: {
            api: 'https://content.jwplatform.com/libraries/QzXs2BlW.js',
            licenseKey: 'imB2/QF0crMqHks7/tAxcTRRjnqA9ZwxWQ2N1A=='
        }
    });
    var lightboxInlineIframe = GLightbox({
        'selector': 'glightbox4'
    });


    function get_gallery_items(firstLoad) {
        console.log(firstLoad);
        $.ajax({
            url: "get-gallery.php",
            type: 'POST',
            data: {
                contenttype: filter,
                start: $(".grid-item").length,
                number: itemsNumber
            }
        }).done(function (data) {
            var gallery = JSON.parse(data);
            $.each(gallery, function (index, val) {
                var $htmlgrid = '<div class="grid-item">' + val.category + '<a href="' + val.src + '" class="glightbox2" data-glightbox="title: Description Left; descPosition: left;"><img src="' + val.src + '" alt="image"><div class="glightbox-desc"><p>' + val.description + '</p></div></a></div>';
                $(".grid").prepend($htmlgrid);

                if ((index + 1) == gallery.length && firstLoad == true) {
                        $grid = $('.grid').imagesLoaded(function () {
                            // init Masonry after all images have loaded
                            $('.grid').masonry({
                                // set itemSelector so .grid-sizer is not used in layout
                                itemSelector: '.grid-item',
                                // use element for option
                                columnWidth: '.grid-sizer',
                                percentPosition: true,
                                gutter: '.gutter-sizer',
                                stagger: 30,
                            })
                        });
                    //                        if (firstLoad === true) {
                } else if (firstLoad == false) {
//                    $(".grid").append($htmlgrid);
//                    $( ".grid" ).masonry( 'reloadItems' );
//                    $( ".grid" ).masonry( 'layout' );
                }
            });
        });
    };
    
    $("#btn-load").click(function () {
        get_gallery_items();
    });

});

    

        function masonry_home(grid, gridCell, gridGutter, dGridCol, tGridCol, mGridCol) {
            var g = document.querySelector(grid),
                gc = document.querySelectorAll(gridCell),
                gcLength = gc.length,
                gHeight = 0,
                i;

            for (i = 0; i < gcLength; ++i) {
                gHeight += gc[i].offsetHeight + parseInt(gridGutter);
            }

            if (window.screen.width >= 1024)
                g.style.height = gHeight / dGridCol + gHeight / (gcLength + 1) + "px";
            else if (window.screen.width < 1024 && window.screen.width >= 768)
                g.style.height = gHeight / tGridCol + gHeight / (gcLength + 1) + "px";
            else
                g.style.height = gHeight / mGridCol + gHeight / (gcLength + 1) + "px";
        }
        var masonryElem = document.querySelector('.masonry3');
    

        ["resize", "load"].forEach(function(event) {
            // Hiding the preloader
            masonryElem.style.display = "none";
            window.addEventListener(event, function() {
                imagesLoaded(document.querySelector('.masonry3'), function() {
                    masonryElem.style.display = "flex";
 
                    // A masonry grid with 8px gutter, with 3 columns on desktop, 2 on tablet, and 1 column on mobile devices.
                    masonry_home(".masonry3", ".masonry-brick3", 0, 3, 2, 1);
                });
            }, false);
        });


