<?php
echo "


<div class='nav-admin'>
  <a class='a-logo-admin' href=\"?module=Accueil\"><img class='logo-admin' src='./include/img/logo-normal-il.png' alt='logo'/></a>
  <div class=\"dropdown\">
    <button class=\"dropbtn\">Accueil
    </button>
    <div class=\"dropdown-content\">
      <a href=\"?module=modif_Acueil\">Contenu</a>
    </div>
  </div> 
  
  <div class=\"dropdown\">
    <button class=\"dropbtn\">A Propos
    </button>
    <div class=\"dropdown-content\">
      <a href=\"?module=form_modif_post_propos\">Contenu</a>
      <a href=\"?module=formulaire_equipe\">Equipe</a>
    </div>
  </div> 
  <div class=\"dropdown\">
    <button class=\"dropbtn\">Service
    </button>
    <div class=\"dropdown-content\">
      <a href=\"?module=form_ajout_post_service&service=Danse\">Danse</a>
      <a href=\"?module=form_ajout_post_service&service=Musique\">Musique</a>
      <a href=\"?module=form_ajout_post_service&service=Prive\">Cours privé</a>
      <a href=\"?module=form_ajout_post_service&service=Planning\">Planning</a>
    </div>
  </div> 
  
  <div class=\"dropdown\">
    <button class=\"dropbtn\">Galerie
    </button>
    <div class=\"dropdown-content\">
      <a href=\"?module=admin_photo\">Photo</a>
      <a href=\"?module=admin_video\">Vidéo</a>
    	<a href=\"?module=ajouter_tag\">Ajouter Tag</a>
      <a href=\"?module=form_supprimer_tag\">Supprimer Tag</a>
    </div>
  </div> 
  
  
  <div class=\"dropdown\">
    <button class=\"dropbtn\">Admin
    </button>
    <div class=\"dropdown-content\">
      <a href=\"?module=form_mdp\">Changer Mdp</a>
      <a href=\"?module=deconnexion\">Déconnexion</a>
    </div>
  </div> 
  
</div>

 
</nav><div class=\"clear\"></div>

";