<footer id="footer">
    <div class="blocks-container">
        <div class="blocks-list">
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">

                            <div class="logo-white">
                                <a href="index.php"><img src="img/Indian-lotus-white.svg" alt="logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p>94 rue Jean Jaurès <br> 93139 Noisy Le Sec</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p><a href="tel:+07 67 81 07 35" title="07 67 81 07 35">07 67 81 07 35</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <a href="mailto:contact@indianlotus.com" title="Envoyer un email à contact@indianlotus.com">contact@indianlotus.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <ul style="visibility: visible;">
                                <div class="icon-wrapper"><i class="fa fa-facebook-f custom-icon"></i></div>
                                <div class="icon-wrapper"><i class="fa fa-instagram custom-icon"></i></div>
                                <div class="icon-wrapper"><i class="fa fa-youtube-play custom-icon"></i></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <i class="clear"></i>
        </div>
    </div>
    <div class="last-links">
        <p>© 2018 Indian Lotus</p><a href="mentions-legales.php">Mentions légales</a> | <a href="sitemap.xml" target="_blank">Plan du site</a>
    </div>
</footer>

<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="js/scrollspy.min.js"></script>
<script src="js/glightbox.min.js"></script>
<script src="js/script.js"></script>
<script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });
      }
    $(document).scroll(function() {
        var row = $('#container-navbar'),
            scrollTop = $(this).scrollTop();
        if (scrollTop > 1) {
            row.addClass('floating-nav');
        } else if (scrollTop <= 1) {
            row.css({
                "box-shadow": "none"
            });
            row.css({
                "opacity": "1"
            });
        }
    });


    function masonry(grid, gridCell, gridGutter, dGridCol, tGridCol, mGridCol) {
        var g = document.querySelector(grid),
            gc = document.querySelectorAll(gridCell),
            gcLength = gc.length,
            gHeight = 0,
            i;

        for (i = 0; i < gcLength; ++i) {
            gHeight += gc[i].offsetHeight + parseInt(gridGutter);
        }

        if (window.screen.width >= 1024)
            g.style.height = gHeight / dGridCol + gHeight / (gcLength + 1) + "px";
        else if (window.screen.width < 1024 && window.screen.width >= 768)
            g.style.height = gHeight / tGridCol + gHeight / (gcLength + 1) + "px";
        else
            g.style.height = gHeight / mGridCol + gHeight / (gcLength + 1) + "px";
    }
    var masonryElem = document.querySelector('.masonry');
    masonryElem.insertAdjacentHTML("afterend", "Loading...");
    var masonryPreloader = document.querySelector('.masonry-preloader');

    ["resize", "load"].forEach(function(event) {
        // Hiding the preloader
        masonryElem.style.display = "none";
        window.addEventListener(event, function() {
            imagesLoaded(document.querySelector('.masonry'), function() {
                masonryElem.style.display = "flex";
                masonryPreloader.style.display = "none";
                // A masonry grid with 8px gutter, with 3 columns on desktop, 2 on tablet, and 1 column on mobile devices.
                masonry(".masonry", ".masonry-brick", 8, 3, 2, 1);
                console.log("Loaded");
            });
        }, false);
    });

    function openNav() {
        document.getElementById("nav-mobile").style.height = "100%";
        document.getElementsByTagName("body")[0].style = "overflow-y: hidden";
    }

    function closeNav() {
        document.getElementById("nav-mobile").style.height = "0%";
        document.getElementsByTagName("body")[0].style = "overflow-y: auto";
    }

    var $parallaxElement = $('.parallax-bg');
    var elementHeight = $parallaxElement.outerHeight();

    function parallax() {

        var scrollPos = $(window).scrollTop();
        var transformValue = scrollPos / 40;
        var opacityValue = 1 - (scrollPos / 2000);
        var blurValue = Math.min(scrollPos / 100, 3);

        if (scrollPos < elementHeight) {

            $parallaxElement.css({
                'transform': 'translate3d(0, -' + transformValue + '%, 0)',
                'opacity': opacityValue,
                '-webkit-filter': 'blur(' + blurValue + 'px)'
            });

        }

    }


    $(window).scroll(function() {
        parallax();
    });
    $(document).ready(function() {

        // display section 1 title in sidebar


        // smooth scrolling
        $(function() {
            $('a[href*="#"]:not([href="#"])').click(function() {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });

        $(".vertical-nav").on("activate.bs.scrollspy", function() {
            var currentItem = $(".vertical-nav li.active > a").text();
            $('#label span').empty().html(currentItem);
        })
        $("#lightSlider").lightSlider();
    });
    
        var lightbox = GLightbox();
    var lightboxDescription = GLightbox({
      selector: 'glightbox2'
    });
    var lightboxVideo = GLightbox({
      selector: 'glightbox3',
      jwplayer: {
        api: 'https://content.jwplatform.com/libraries/QzXs2BlW.js',
        licenseKey: 'imB2/QF0crMqHks7/tAxcTRRjnqA9ZwxWQ2N1A=='
      }
    });
    var lightboxInlineIframe = GLightbox({
      'selector': 'glightbox4'
    });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap"
    async defer></script>
</body>

</html>
