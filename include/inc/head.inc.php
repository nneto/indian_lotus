<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>
        <?= ucfirst(substr($_SERVER['SCRIPT_NAME'],(strrpos($_SERVER['SCRIPT_NAME'],'/') + 1),-4)); ?>
    </title>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/grid.css">
    <link type="text/css" rel="stylesheet" href="css/lightslider.css" />
    <meta name="description" content="Site web de l'association Indian Lotus">
    <meta name="robots" content="">
    <meta name="viewport" content="width=device-width initial-scale=1">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">    
    <link rel="stylesheet" href="css/glightbox.min.css">
    <link rel="stylesheet" href="css/style.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="js/lightslider.min"></script>

    <!--[if lte IE 9]>
        <script src="masonry.pkgd.min.js"></script>
        <![endif]-->
</head>

<body data-spy="scroll" data-target="#vertical-menu">
    <?php
	/*
		$derniereOccurence = strrpos($_SERVER['SCRIPT_NAME'],'/'); => 15
		$intituleFichier = substr($_SERVER['SCRIPT_NAME'],16,-4); => contact
		$fin = ucfirst(substr($_SERVER['SCRIPT_NAME'],(strrpos($_SERVER['SCRIPT_NAME'],'/') + 1),-4));
	*/

	?>
