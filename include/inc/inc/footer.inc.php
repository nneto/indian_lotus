<footer id="footer">
    <div class="blocks-container">
        <div class="blocks-list">
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">

                            <div class="logo-white">
                                <a href="index.php"><img src="img/Indian-lotus-white.svg" alt="logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-adresse">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p>94 rue Jean Jaurès <br> 93130 Noisy Le Sec</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-tel">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p><a href="tel:+07 67 81 07 35" title="07 67 81 07 35">07 67 81 07 35</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-mail">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <a href="mailto:contact@indianlotus.com" title="Envoyer un email à contact@indianlotus.com">contact@indianlotus.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <ul style="visibility: visible;">
                                <div class="icon-wrapper"><a href=""><i class="fa fa-facebook-f custom-icon"></i></a></div>
                                <div class="icon-wrapper"><a href=""><i class="fa fa-instagram custom-icon"></i></a></div>
                                <div class="icon-wrapper"><a href=""><i class="fa fa-youtube-play custom-icon"></i></a></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="last-links">
        <p>© 2018 Indian Lotus</p><a href="mentions-legales.php">Mentions légales</a> | <a href="sitemap.xml" target="_blank">Plan du site</a>
    </div>

    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollspy.min.js"></script>
    <script src="js/masonry.pkgd.min.js"></script>
    <script src="js/lightslider.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="js/glightbox.min.js"></script>
    <script src="js/jquery.endless-scroll.js"></script>

    <script src="js/script.js"></script>
    </body>

    </html>
