<div id="container-navbar">
    <!--     <div class="btn-menu-block">     -->
    <!-- </div> -->
    <div class="logo">
        <a href="index.php"><img class="desktop-logo" src="img/logo-normal-il.png" alt="logo"><img class="mobile-logo" src="img/favicon.png" alt="logo"></a>
    </div>
    <nav class="menu">

        <ul id="nav-desktop">
            <li><a href="about.php"><span class="hover-menu">À PROPOS</span></a></li>
            <li><a href="services.php"><span class="hover-menu">SERVICES</span></a></li>
            <li><a href="galerie.php"><span class="hover-menu">GALERIE</span></a></li>
            <li><a href="contact.php"><span class="hover-menu">NOUS REJOINDRE</span></a></li>
            <li class="mobile-link"><span onclick="openNav()">Menu &#9776;</span></li>
        </ul>
    </nav>
</div>
<div class="spacer">
    &nbsp;
</div>

<div id="nav-mobile" class="overlay">
    <div class="logo-white">
        <a href="index.php"><img src="img/Indian-lotus-white.svg" alt="logo"></a>
    </div>
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <h2 class="screen-center">MENU</h2>
    <div class="overlay-content">
        <a href="about.php"><span class="hover-menu">À PROPOS</span></a>
        <a href="services.php"><span class="hover-menu">SERVICES</span></a>
        <a href="galerie.php"><span class="hover-menu">GALERIE</span></a>
        <a href="contact.php"><span class="hover-menu">NOUS REJOINDRE</span></a>
    </div>
    <div class="footer-menu">
        <div class="coordonnees">
            <div class="email">
                <a href="mailto:contact@indianlotus.com" title="Envoyer un email à contact@indianlotus.com">contact@indianlotus.com</a>
            </div>
            <div class="tel">
                <a href="tel:+07 67 81 07 35" title="07 67 81 07 35">07 67 81 07 35</a>
            </div>
        </div>
        <div class="menu-rs">
            <ul class="social" style="visibility: visible;">
                <div class="icon-wrapper"><a href=""><i class="fa fa-facebook-f custom-icon"></i></a></div>
                <div class="icon-wrapper"><a href=""><i class="fa fa-instagram custom-icon"></i></a></div>
                <div class="icon-wrapper"><a href=""><i class="fa fa-youtube custom-icon"></i></a></div>
            </ul>
        </div>
    </div>
</div>
