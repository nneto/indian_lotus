<?php

echo "    <!-- Optional theme -->
<div id=\"container-navbar\">
    <!--     <div class=\"btn-menu-block\">     -->
    <!-- </div> -->
    <div class=\"logo\">
        <a href=\"?module=Accueil\"><img src=\"./include/img/logo-normal-il.png\" alt=\"logo\"></a>
    </div>
    <nav class=\"menu\">
        
        <ul id=\"nav-desktop\">
            <li><a href=\"?module=propos\"><span class=\"hover-menu\">À PROPOS</span></a></li>
            <li><a href=\"?module=service\"><span class=\"hover-menu\">SERVICES</span></a></li>
            <li><a href=\"?module=Gallery\"><span class=\"hover-menu\">GALERIE</span></a></li>
            <li><a href=\"?module=contact\"><span class=\"hover-menu\">NOUS REJOINDRE</span></a></li>
            <li class=\"mobile-link\"><span style=\"font-size:30px;cursor:pointer\" onclick=\"openNav()\">&#9776; Menu</span></li>
        </ul>
    </nav>
</div>
<div class=\"spacer\">
    &nbsp;
</div>

<div id=\"nav-mobile\" class=\"overlay\">
    <div class=\"logo-white\">
        <a href=\"index.php\"><img src=\"./include/img/Indian-lotus-white.svg\" alt=\"logo\"></a>
    </div>
    <a href=\"javascript:void(0)\" class=\"closebtn\" onclick=\"closeNav()\">&times;</a>
    <h2 class=\"screen-center\">MENU</h2>
    <div class=\"overlay-content\">
        <a href=\"?module=propos\"><span class=\"hover-menu\">À PROPOS</span></a>
        <a href=\"?module=service\"><span class=\"hover-menu\">SERVICES</span></a>
        <a href=\"?module=Gallery\"><span class=\"hover-menu\">GALERIE</span></a>
        <a href=\"?module=contact\"><span class=\"hover-menu\">NOUS REJOINDRE</span></a>
    </div>
</div>";