<!DOCTYPE>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Indian lotus <?php // echo $module->getControleur()->getVue()->getTitre();?></title>
    <link rel="stylesheet" href="include/css/reset.css">
    <link rel="stylesheet" href="include/css/grid.css">
    <script src="jquery-3.1.1.min.js"></script>

    <meta name="description" content="Site web de l'association Indian Lotus">
    <meta name="robots" content="">
    <meta name="viewport" content="width=device-width initial-scale=1">
    <link rel="icon" href="include/img/favicon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="include/css/glightbox.min.css">
  
    <link rel="stylesheet" href="include/css/jquery.bxslider.min.css">

    <link rel="stylesheet" href="include/css/style.css">
   <link rel="stylesheet" href="include/css/style2.css">
    <link rel="stylesheet" href="./css/styleback.css">

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script src="include/default.js"></script>

    <?php
   // $css=$module->getControleur()->getVue()->getCss();
    if(empty($css)){
    }else{
        foreach ($css as $cs){
            echo $cs.
                "\n";
        }
    }
    ?>
</head>
<body>


<?php

if( isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte'])){
    include('include/navAdmin.php');
}else{
    include('include/nav.php');
}

echo $module->getControleur()->getVue()->getContenu();
?>
<footer id="footer">
    <div class="blocks-container">
        <div class="blocks-list">
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">

                            <div class="logo-white">
                                <a href="index.php"><img src="include/img/Indian-lotus-white.svg" alt="logo"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-adresse">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p>94 rue Jean Jaurès <br> 93130 Noisy Le Sec</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="include/img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-tel">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <p><a href="tel:+07 67 81 07 35" title="07 67 81 07 35">07 67 81 07 35</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block block-separator">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <div class="separator">
                                <div class="drop"><img src="include/img/lotus-footer.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block footer-mail">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <a href="mailto:contact@indianlotus.com" title="Envoyer un email à contact@indianlotus.com">contact@indianlotus.com</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="valign-table">
                    <div class="valign-row">
                        <div class="valign-cell">
                            <ul style="visibility: visible;">
                                <div class="icon-wrapper"><a href="https://www.facebook.com/IndianLotusassociation/"><i class="fa fa-facebook-f custom-icon"></i></a></div>
                                <div class="icon-wrapper"><a href=""><i class="fa fa-instagram custom-icon"></i></a></div>
                                <div class="icon-wrapper"><a href=""><i class="fa fa-youtube-play custom-icon"></i></a></div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="last-links">
        <p>© 2018 Indian Lotus</p><a href="?module=legal">Mentions légales</a> | <a href="?module=Accueil" >Plan du site</a>
    </div>
</body>

    <!--
    <footer>
        <div class="centered clearfix">
            <div class="logo-white">
                <a href="index.php"><img src="img/Indian-lotus-white.svg" alt="logo"></a>
            </div>
            <div class="footer-contact">
                <div class="footer-coordonnees">94 rue Jean Jaurès <br> 93139 Noisy Le Sec</div>
                <div class="separator">
                    <div class="drop"><img src="img/lotus-footer.png"></div>
                </div>
                <div class="footer-coordonnees"><a href="tel:+07 67 81 07 35" title="07 67 81 07 35">07 67 81 07 35</a></div>
                <div class="separator">
                    <div class="drop"><img src="img/lotus-footer.png"></div>
                </div>
                <div class="footer-coordonnees"><a href="mailto:contact@indianlotus.com" title="Envoyer un email à contact@indianlotus.com">contact@indianlotus.com</a></div>
            </div>
            <div class="social">
                <div class="icon-wrapper"><i class="fa fa-facebook-f custom-icon"></i></div>
                <div class="icon-wrapper"><i class="fa fa-instagram custom-icon"></i></div>
                <div class="icon-wrapper"><i class="fa fa-youtube-play custom-icon"></i></div>
            </div>
        </div>
        <div class="last-links">
            <p>© 2018 Indian Lotus</p><a href="mentions-legales.php">Mentions légales</a> | <a href="sitemap.xml" target="_blank">Plan du site</a>
        </div>
    </footer>
-->
    <script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="include/js/scrollspy.min.js"></script>
    <script src="include/js/masonry.pkgd.min.js"></script>
    <script src="include/js/lightslider.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script src="include/js/glightbox.min.js"></script>
    <script src="include/js/jquery.endless-scroll.js"></script>

    <script src="include/js/script.js"></script>
    </body>

    </html>


</html>