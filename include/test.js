function afficheBootbox(token) {
    var b = bootbox.dialog({
            size :'medium',
            onEscape: true,
            backdrop:true,
            className:"",
            message:   " <div  >\
                <ul class='nav nav-pills'>\
                    <li data-toggle='pill' class='active' onclick='connexion(\""+token+"\")'> <a>Connexion</a> </li>\
                    <li data-toggle='pill' onclick='inscription(\""+token+"\")' > <a>Inscription</a> </li>\
                </ul>\
                <div id='form' >\
                    <form style='margin-top: 1%'  class='form-horizontal' action='index.php?module=connexion' method='post'>\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3 ' for='pseudo'> pseudo</label>\
                        <div class='col-sm-8'>\
                            <input style='width: 100%' class='form-control ' type='text' id='pseudo'  name='pseudo' required> </div>\
                        </div>\
                        <div class='form-group'>\
                            <label for='mdp' class='control-label col-sm-3'>mot de passe</label> \
                            <div class='col-sm-8'>\
                                <input class='form-control' type='password' name='mdp' id='mdp' required><br/>\
                                <a href='index.php?module=mdp'> Mot de passe oublier </a>\
                            </div>\
                        </div>\
                        <input type='submit' class='btn btn-primary btn-responsive' value='se connecter'>\
                 </div>\
                  </form>\
                </div>\
             </div> \ "
        }
        , function(result) {
            if(result)
                $('#infos').submit();
        })
    b.find('.modal-content').addClass("col-ms-8");
}

function connexion(token) {
    $('#form').html(" \
                <form style='margin-top: 1%'  class='form-horizontal' action='index.php?module=connexion' method='post'>\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3 ' for='pseudo'> pseudo</label>\
                        <div class='col-sm-8'>\
                            <input style='width: 100%' class='form-control ' type='text' id='pseudo'  name='pseudo' required> </div>\
                        </div>\
                        <div class='form-group'>\
                            <label for='mdp' class='control-label col-sm-3'>mot de passe</label> \
                            <div class='col-sm-8'>\
                                <input class='form-control' type='password' name='mdp' id='mdp' required>\
                                <a href='index.php?module=mdp'> Mot de passe oublier </a>\
                            </div>\
                        </div>\
                        <input type='submit' class='btn btn-primary btn-responsive' value='se connecter'>\
                 </div>\
                  </form>\
                  ");
}

function inscription(token) {
    $('#form').html(" \
     <form style='margin-top: 1%' class='form-horizontal' action='index.php?module=inscription' method='post'>\
        <input type='hidden' name='token' value='"+token+"'>\
           <div class='form-group'>\
                <label class='control-label col-sm-3' for='nom'> nom</label> \
                <div class='col-sm-8'>\
                      <input class='form-control' type='text' name='nom' id='nom' required>\
                </div>\
           </div>\
           <div class='form-group'>\
                <label class='control-label col-sm-3' for='prenom'>prenom</label>\
                <div class='col-sm-8'>\
                     <input class='form-control' type='text' name='prenom' id='prenom' required>\
                </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='pseudo'>pseudo</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='text' name='pseudo' id='pseudo' required>\
               </div>\
           </div>\
            <div class='form-group'>\
                  <label class='control-label col-sm-3' for='email'>email</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='email' name='email' id='email' required>\
               </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='mdp'>mot de passe</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='password' name='mdp' id='mdp' required>\
               </div>\
           </div>\
           <div class='form-group'>\
                  <label class='control-label col-sm-3' for='mdp2'>confirmer le mot de passe</label>\
               <div class='col-sm-8'>\
                   <input class='form-control' type='password' name='mdp2' id='mdp2s' required>\
               </div>\
           </div>\
        <input type='checkbox' value='cgu' name='cgu' required>  Cocher cette case pour accepter les <a href='index.php?module=CGU' target='_blank'>  condition d'utilisation</a><br/> \
        <input type='submit' class='btn btn-primary center-block' value='inscription'>\
      </form>");
}


function afficheBootboxDepotPhoto(token,idcompte) {

    $.ajax({
        url : 'module/mod_vote/aVoter.php',
        type:'POST',
        data: 'idcompte='+idcompte,
        dataType:'html',
        onEscape: true,
        backdrop:true,
        className:"",

        success : function(code_html){

            console.log(code_html);
            if(code_html=="true"){
                var b = bootbox.dialog({
                        size :'medium',
                        onEscape: true,
                        backdrop:true,
                        className:"",
                        message:   " <div>\
                <div id='form' >\
                <h1>Déposer votre photo</h1>\
                <form class='form-horizontal' action='index.php?module=deposer' method='post' enctype='multipart/form-data' >\
                    <input type='hidden' name='token' value='"+token+"'>\
                    <div class='form-group'>\
                        <label class='control-label col-sm-3' for='titre'>Titre</label>\
                        <div class='col-sm-8'>\
                             <input class='form-control' type='text' name='titre' id='titre' required>\
                        </div>\
                    </div>\
                     <div style='margin-left: 15%' class='form-group'>\
                        <input type='file' name='fichImagePhoto'>\
                    </div>\
                     <input type='submit' class='btn btn-primary btn-responsive' value='déposer'>\
                 </form>\
                </div>\
             </div> "
                    }
                    , function(result) {
                        if(result)
                            $('#infos').submit();
                    })
                b.find('.modal-content').addClass("col-ms-8");

            }else{
                var b = bootbox.dialog({
                        size :'medium',
                        onEscape: true,
                        backdrop:true,
                        className:"",
                        message:   "<h1>Vous avez  déjà deposer une photo</h1> "
                    }
                    , function(result) {
                        if(result)
                            $('#infos').submit();
                    })
            }
        }});

}