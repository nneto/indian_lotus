<?php

class ModuleGenerique
{
    protected $controleur;

    /**
     * @return mixed
     */
    public function getControleur()
    {
        return $this->controleur;
    }


    public function vue_erreur($msg){
        $this->controleur->vue_erreur($msg);
    }
    
    public function isAdmin(){
        return isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte']);
    }
}