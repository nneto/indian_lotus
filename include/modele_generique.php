 <?php

class ModeleGenerique
{
    private static $dns= "mysql:host=localhost;dbname=indianLotus";
    private static $usr= "root";
    private static $psw="root";

    protected  static  $connexion ;

    public static function  init(){
       self::$connexion=new PDO(self::$dns,self::$usr,self::$psw);
       self::$connexion->exec("SET NAMES 'UTF8'");
    }
    public  function mdpCrypt($message,$login){
        $salt="jnrth4r8t4a464erarzvsd";
        $salt2=substr($salt,0,strlen($salt)-strlen($login));
        $message.=$salt;
        return  hash('sha256',$message);
    }

    public function createToken($validiter ="300"){
        $token=$this->random(20);
        $r=self::$connexion->prepare("insert into token VALUES(?,now(),?)");
        $r->execute(array($token,$validiter));
        return $token;
    }

    public function getToken($token){
        $r=self::$connexion->prepare("select * from token where TIMESTAMPDIFF(SECOND,creation,now())<expiration  and token =?");
        $r->execute(array($token));

        $t= $r->fetch(PDO::FETCH_ASSOC);
        $this->effacerToken($token);
        return $t;
    }

    public function effacerToken($token){
        $r=self::$connexion->prepare("delete from token  where token =?");
        $r->execute(array($token));
        $this->effacerTokenNonValide();
    }

    public function effacerTokenNonValide(){
        $r=self::$connexion->prepare("delete from token where TIMESTAMPDIFF(SECOND,creation,now())>expiration ");
        $r->execute();
    }
    public function random($car) {
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    public function getPost($nomPage){
        $r=self::$connexion->prepare("Select * from page where nom_page=?");
        $r->execute(array($nomPage));
        $page=$r->fetchall(PDO::FETCH_ASSOC);
        $r=self::$connexion->prepare("Select * from post inner join page_relation_post prp ON post.id_post = prp.id_post where prp.id_page=?");
        $r->execute(array($page[0]['id_page']));
        return $r->fetchall(PDO::FETCH_ASSOC);
    }
}