<?php


require_once('vue_admin.php');
require_once ('modele_admin.php');
class ControleurAdmin extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new Vueadmin();
        $this->modele=new ModeleAdmin();
    }
    
    public function admin_accueil(){
        $this->vue->admin_accueil();
    }

    public function affiche_formulaire(){

       $token= $this->modele->createToken();
        $this->vue->vue_admin_formulaire($token);
    }

    public function admin_err($err){
        $token= $this->modele->createToken();
        $this->vue->vue_err_admin($token,$err);
    }

    public function connexion($user,$psw,$submit,$token){

        $compte=$this->modele->getCompte($user,$psw);

        if($compte == NULL){
            $this->modele->effacerToken($token);
            var_dump($this->modele->mdpCrypt($psw,$user));
            //header('Location:index.php?module=accueil&err=Identifiants%20incorrect');
        }else if($compte[0]['activer']=="0"){
            $this->modele->effacerToken($token);
            header('Location:index.php?module=accueil&err=Compte%20non%20activé%20vérifiez%20vos%20mails');
        }else{
            $this->modele->effacerToken($token);
            var_dump($compte);
            $_SESSION['idcompte']=$compte[0]['idUser'];
            $_SESSION['email']=$compte[0]['Email'];
            $_SESSION['isSuperAdmin']=$compte[0]['isSuperAdmin'];
            var_dump($_SESSION);
        }
    }
}