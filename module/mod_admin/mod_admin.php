<?php


include_once('controleur_admin.php');
class ModAdmin extends ModuleGenerique
{
    public function module_admin(){
        $this->controleur= new ControleurAdmin();
        $this->controleur->admin();
    }

    public function module_err_accueil($err){
        $this->controleur= new ControleurAdmin();
        $this->controleur->admin_err($err);
    }

    public function module_formulaire_admin(){
        
        $this->controleur=new ControleurAdmin();
        $this->controleur->affiche_formulaire();
    }

    public function module_connexion_admin($tab){


        $this->controleur=new ControleurAdmin();


        if(isset($tab['token']) && isset($tab['user']) && isset($tab['mdp']) && isset($tab['submit'])){
            $user=htmlspecialchars($tab['user']);
            $psw=htmlspecialchars($tab['mdp']);
            $submit=htmlspecialchars($tab['submit']);
            $token=$tab['token'];
            $this->controleur->connexion($user,$psw,$submit,$token);
        }else{

        }
    }
}
