<?php
class VueAdmin extends VueGenerique
{
    public function vue_Admin_formulaire($token)
    {
        $this->titre = "Connexion";
        $this->Css = array("<link href=\"\" rel=\"\">");
        $this->contenu = "
            <form method='POST' action='?module=admin_connexion'>
            <input type='hidden' name='token' value='$token' />
            <label for='user'>Utilisateur</label>
            <input type='text' id='user' name='user' min='7' maxlength='100' required>
            <label for='mdp'>Mot de passe</label>
            <input type='password' id='mdp' name='mdp' min='7' maxlength='100' required>
            <a href='?module=mdp_Oublier'>Mot de passe Oublié</a> 
            <input type='submit' name='submit' value='connexion'>  
        </form>

        ";
    }
    
    public function admin_accueil(){
        $this->titre = "Admin";
        $this->Css = array("<link href=\"\" rel=\"\">");
        $this->contenu = "Lapin";
    }

}
