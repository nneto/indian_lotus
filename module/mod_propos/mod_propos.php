<?php

include_once('controleur_propos.php');
class ModPropos extends ModuleGenerique
{
    public function module_propos(){
        $this->controleur= new ControleurPropos();
        $this->controleur->propos();
    }

    public function module_err_propos($err){
        $this->controleur= new ControleurPropos();
        $this->controleur->accueil_err($err);
    }
    public function module_forumaire_modif_propos(){
        if($this->isAdmin()){
        
        $this->controleur = new ControleurPropos();
        $this->controleur->controlleur_forumaire_modif_Propos();
        }
        
    }



}