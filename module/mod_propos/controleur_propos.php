<?php

require_once('vue_propos.php');
require_once ('modele_propos.php');
class ControleurPropos extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VuePropos();
        $this->modele=new ModelePropos();
    }

    public function propos(){

        $posts=$this->modele->getPostAndPhoto();
        $photos=$this->modele->getPhotos();
        $equipe=$this->modele->getEquipe();
        $this->vue->vue_Propos($posts ,$photos,$equipe);
    }

    public function accueil_err($err){
        $token= $this->modele->createToken();
        $this->vue->vue_err_Accueil($token,$err);
    }


    public function controlleur_forumaire_modif_Propos(){
        $token= $this->modele->createToken();
        $posts= $this->modele->getPostAndPhoto();
        $photos= $this->modele->getPhotos();
        $this->vue->vue_formulaire_modif_Propos($token,$posts,$photos);
    }



}