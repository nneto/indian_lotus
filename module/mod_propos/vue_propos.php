<?php
class VuePropos extends VueGenerique
{
    public function vue_Propos($posts,$photo,$equipe)
    {
        $this->titre = "A propos";
        $this->Css = array("<link href=\" \" rel=\"\">");
        $this->contenu .= " <header>   
        <div class=\"header-content\" id=\"header-about\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"text-white text-center\">L'association</h2>
                </div>
            </div>
        </div>
    </div>
</header>";


        foreach ($posts as $post) {
            if ($post['nom_post'] == "Qui") {
              $this->contenu .= "<section id=\"whoweare\">
    <div class=\"container\">
        <h2 class=\"title-fantasy text-black\">$post[titre_post]</h2>
        <div class=\"bookmark\"></div>
    </div>
    <div class=\"container\">
        <div class=\"text-center\">
            <p>$post[description_post]</p>
        </div>
        <a class=\"btn  btn-color\" href=\"?module=contact\">$post[text_button_post]</a>
    </div>
</section>
";    
            }

        }
        $this->contenu.="<section id=\"equipe\" class=\"about-container style1\">
    <div class=\"inner\">
        <div class=\"container align-center\">
            <h2 class=\"title-fantasy text-black text-center\">Notre équipe</h2>
            <div class=\"bookmark bookmark-center\"></div>
        </div>
        <div class=\"flex flex-4\">";
        
          
        foreach ($equipe as $membre){
        $this->contenu.="
         
        <div class= \"col text-center\">
                <div class=\"image round fit\">
                    <img src='$membre[cheminImage]' alt=\"0\">
                </div>
                <h4>$membre[prenom] $membre[nom]</h4>
                <h5>$membre[status]</h5>
                <p>$membre[description]
                </p>
            </div>
        ";
        }
   $this->contenu.="   </div>
</section>";
        foreach ($posts as $post) {
            if ($post['nom_post'] == "Cause") {
                $this->contenu .= "<section id=\"cause\">
        <div class=\"container\">
            <h2 class=\"title-fantasy\">$post[titre_post]</h2>
            <div class=\"bookmark\"></div>
             </div>
              <div class=\"clear\"></div>
             <div class=\"container\">
            <div class=\"text-content\">
                <p class=\"description-text text-white\">$post[description_post]</p>
                <div class=\"encadre-text\">
                <a href='$post[text_button_post]'><p>Pour plus d'information: cliquer ICI</p></a>
                </div>
            </div>
        </div>    
    </section>
";
            }
        }
    }

    public function vue_formulaire_modif_Propos($token, $posts, $photos)
    {
        
        
          $this->contenu.="
          <section class='section-modif-accueil'>
        <section class=\"bo-contener\">
    		<div class=\"headPost\">
    	  		<p  class=\"page-titre title-fantasy \">Post de la page a Propos</p>
    	  		
    	  		<button class=\"buttonVisualiser\" onclick='document.location.href=\"?module=propos\"'>Visualiser la page</button>
    	  	</div>";
        
    
        foreach ($posts as $post){
            
        $this->contenu.=" 
    	  	<div class=\"post\">
    	  		<div class=\"form-style-2\">
    				<div class=\"form-style-2-heading\"><img src=\"./$post[chemin]\" class=\"img-post\" onclick='modif_photo_post_service($post[id_post])' ></div>
    				<form action='?module=modif_post_Acueil'  method=\"post\" id='form$post[id_post]'>
    	                <input type='hidden' name='token' value='$token' />
    	  <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=confirm' />
                        <input type='hidden' name='id_post' required value='$post[id_post]'></input>
    					<label for=\"field1\"><span>Titre <span class=\"required\">*</span></span><input type='text' name='titre_post' required value='$post[titre_post]'></input></label>
    					<label for=\"field2\"><span>Sous-titre <span class=\"required\">*</span></span><input type=\"text\" class=\"input-field\" name=\"sous-titre\" value='$post[sous_titre]'/></label>
    					<label for=\"field3\"><span>Description <span class=\"required\">*</span></span><textarea form='form$post[id_post]' name='description' class=\"textarea-field\">$post[description_post]</textarea></label>
    					<span>&nbsp;</span><input type=\"submit\" value=\"Modifier\" /><span></span>
    				</form>
    			</div>
    	  	</div>
  	   
  	";
            }
           $this->contenu.="
           
<div id=\"myModal\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
  <span class=\"close\">&times;</span>
  <h1 class='title-fantasy text-white'>Ajouter Une Image</h1>
    
  <form method='post' action='?module=ajout_Photo_post' id='form' enctype=\"multipart/form-data\">

                      <input type='hidden' name='token' value='$token'/>
                      <input type='hidden' name='id_post' class='input-hidden-id-post' value='0'/>
                     <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=confirm' />     
                      <input type='file' class='input-file-modal' name='photo' accept='.jpg, .jpeg, .png'/>
                      
                      <label class='text-white'>Titre Photo
                          <input type='text' name='titre_photo'></input>
                      </label>
                      <br/>
                      <label class='text-white' >Description
                          <textarea name='description' form='form'></textarea>
                      </label>
                      <input type='submit' value='valider'>
                </form>";
                
            foreach ($photos as $photo) {
                $this->contenu.="
                    <img src=\" ./$photo[chemin]\" style=' width:200px ' onclick='modif_photo_post($photo[id_photo],$post[id_post])'>";
            }
            
             $this->contenu.="
                <form method='post' class='form_modif_photo_existant' action='?module=modif_photo_existant'>
                  <input type='hidden' name='id_post' class='input-hidden-id-post' value='0'/>
                  <input type='hidden' name='id_photo' class='input-hidden-id-photo' value='0'/>
                  <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=confirm' />
                </form>       
                </div>
                </div>";
        $this->contenu.='<div class="clear"></div></section> </section>';
         if(isset($_GET['form'])){
            $this->contenu.="<script>alert('Le post a bien été modifié');</script>";
        }
   
}
}
