<?php

class  ModeleTag extends ModeleGenerique{

    public function ajouterTag($nom){
        $r=self::$connexion->prepare("insert into tag VALUES (DEFAULT ,?)");
        $s=$r->execute(array($nom));
    }
    public function getTags(){
        $r=self::$connexion->prepare("select * from tag");
        $s=$r->execute();
        return $r->fetchAll(PDO::FETCH_ASSOC);
    }


    public function ajouter_Tag_Photo($id_tag,$id_photo){
        $r=self::$connexion->prepare("insert into photo_relation_tag VALUES (? ,?)");
        $s=$r->execute(array($id_photo,$id_tag));
    }

    public function supr_Tag($id_tag){
        
        $r=self::$connexion->prepare("delete from tag where id_tag=?");
        $s=$r->execute(array($id_tag));
    }
}
