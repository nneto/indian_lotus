<?php
require_once('modele_tag.php');
require_once('vue_tag.php');

class ControleurTag extends ControleurGenerique
{
    /**
     * ControleurAjoutPhoto constructor.
     */
    public function __construct()
    {
        $this->modele= new ModeleTag();
        $this->vue= new VueTag();
	
    }

    public function ajouterTag(){
        $nom = $_POST['nom_tag'];

        $this->modele->ajouterTag($nom);
         header("Location: ?module=admin_photo");

    }
    public function affiche_formulaire_Tag(){
        $this->vue->affiche_formulaire_Tag();
    }

    public function ajouter_Tag_Photo($tab){
        $id_tag=$tab['tag'];
        $id_photo=$tab['id_photo'];
        $this->modele->ajouter_Tag_Photo($id_tag,$id_photo);
        header("Location: ?module=admin_photo");
    }


    public function affiche_formulaire_supprimer_Tag(){
            $tags=$this->modele->getTags();
            $this->vue->affiche_formulaire_supprimer_Tag($tags);
            
    }

    public function supr_Tag($tag){
            $tags=$this->modele->supr_Tag($tag);
            header("Location: ?module=form_supprimer_tag");     

    }
}
