<?php
require_once('controlleur_tag.php');
class ModuleTag extends ModuleGenerique{


    /**
     * ModuleAffichePhoto constructor.
     */
    public function __construct()
    {
        $this->controleur= new ControleurTag();
    }

    public function affiche_formulaire_Tag(){
        $this->controleur->affiche_formulaire_Tag();
    }

    public function affiche_formulaire_supprimer_Tag(){
        $this->controleur->affiche_formulaire_supprimer_Tag();
    }


    public function ajouter_Tag(){
        if($this->isAdmin()){
            if(isset($_POST['nom_tag'])){
                $this->controleur->ajouterTag();
            }    
        }
    }


    public function ajouter_Tag_Photo(){
        if($this->isAdmin()){
            if(isset($_POST['tag']) && isset($_POST['id_photo'])){
                $this->controleur->ajouter_Tag_Photo($_POST);
            }    
        }        
    }

    public function supr_Tag(){
                $this->controleur->supr_Tag($_POST['tag']);
    }
}
