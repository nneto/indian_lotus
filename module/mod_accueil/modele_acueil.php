<?php

class ModeleAcueil extends ModeleGenerique
{
    public function getLastPhoto($nb){
        $r=self::$connexion->prepare("select * from photo ORDER BY datePhoto DESC LIMIT $nb");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function getPhotos(){
        $r=self::$connexion->prepare("select * from photo ");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function getPostAndPhoto(){
        $r=self::$connexion->prepare("select * from post 
        INNER JOIN photo p ON post.id_photo = p.id_photo 
        INNER JOIN page_relation_post prp ON post.id_post = prp.id_post
        INNER JOIN page p2 ON prp.id_page = p2.id_page
        where p2.nom_page= 'accueil';");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }
    public function modif_post_accueil($id,$nom,$sous_titre,$description){
        $r=self::$connexion->prepare("update post set titre_post=? ,sous_titre=? , description_post =? where id_post=?");
        $s=$r->execute(array($nom,$sous_titre,$description,$id));

    }

    public function ajouter_photo_post($chemin,$titre,$description,$id){

        var_dump($chemin,$titre,$description,$id);
        $r=self::$connexion->prepare("insert into photo VALUES (DEFAULT ,?,?,?,?)");
        $r->execute(array($chemin,$titre,$description,time()));
        $id_photo=self::$connexion->lastInsertId();
        $r=self::$connexion->prepare("update post set id_photo=? where id_post=?");
        $a=$r->execute(array($id_photo,$id));
    }


    public function modif_photo_existant($id_post,$id_photo){
        var_dump($id_post,$id_photo);
        $r=self::$connexion->prepare("update post set id_photo=? where id_post=?");
        $a=$r->execute(array($id_photo,$id_post));
    }
}
