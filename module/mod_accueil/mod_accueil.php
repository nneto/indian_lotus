<?php


include_once('controleur_accueil.php');
class ModAccueil extends ModuleGenerique
{
    public function module_accueil(){
        $this->controleur= new ControleurAccueil();
        $this->controleur->accueil();
    }

    public function module_err_accueil($err){
        $this->controleur= new ControleurAccueil();
        $this->controleur->accueil_err($err);
    }
    public function module_forumaire_modif_accueil(){
        if($this->isAdmin()){
            $this->controleur = new ControleurAccueil();
        $this->controleur->controlleur_forumaire_modif_accueil();    
        }
        
    }

    public function module_modif_post_accueil($tab){
        if($this->isAdmin()){
            if(isset($tab['id_post']) && isset($tab['titre_post']) && isset($tab['description'])){
                $this->controleur = new ControleurAccueil();
                $this->controleur->controlleur_modif_post_accueil($tab);
            }else{
                var_dump("erreur");
            }    
        }
    }

    public function module_ajout_photo_post($tab){
        var_dump($_FILES);
        if($this->isAdmin()){
            if(isset($tab['token']) && isset($tab['id_post']) && isset($tab['titre_photo']) && isset($tab['description']) && isset($_FILES['photo']) ){
                $this->controleur = new ControleurAccueil();
                $this->controleur->controlleur_ajout_post_accueil($tab);
            }else{
            
            }    
        }
        
    }

    public function modif_photo_existant($tab){
        if($this->isAdmin()){
            if(isset($tab['id_post']) && isset($tab['id_photo'])){
                $this->controleur = new ControleurAccueil();
                $this->controleur->modif_photo_existant($tab);
            }else{
                var_dump('erreur');
            }
        }
    }

    public function mention(){
                    $this->controleur= new ControleurAccueil();
    
                $this->controleur->mention();
    }
}