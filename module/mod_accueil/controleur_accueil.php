<?php


require_once('vue_accueil.php');
require_once ('modele_acueil.php');
class ControleurAccueil extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueAccueil();
        $this->modele=new ModeleAcueil();
    }

    public function accueil(){

        $posts=$this->modele->getPost($_GET['module']);
        $photos=$this->modele->getPhotos();
        $lastPhotos=$this->modele->getLastPhoto("6");
        $this->vue->vue_Accueil($posts ,$lastPhotos,$photos);
    }

    public function accueil_err($err){
        $token= $this->modele->createToken();
        $this->vue->vue_err_Accueil($token,$err);
    }

    public function controlleur_forumaire_modif_accueil(){
        $token= $this->modele->createToken();
        $posts= $this->modele->getPostAndPhoto();
        $photos= $this->modele->getPhotos();
        $this->vue->vue_formulaire_modif_Accueil($token,$posts,$photos);
    }
    

    public function controlleur_modif_post_accueil($tab){

        $id=$tab['id_post'];
        $nom=$tab['titre_post'];
        $sous_titre=$tab['sous-titre'];
        $description=$tab['description'];
        $url=$tab['url'];
        $this->modele->modif_post_accueil($id,$nom,$sous_titre,$description);
        header("Location: $url");
    }

    public function controlleur_ajout_post_accueil($tab){
        $token=$tab['token'];
        $id=$tab['id_post'];
        $nom=$tab['titre_photo'];
        $description=$tab['description'];
        $titre=$tab['titre_photo'];
        $description=$tab['description'];
         
        $target_dir = "image/";
        $target_file = $target_dir . basename(time()."".$_FILES["photo"]["name"]);
        
// Remove any characters you don't want
// The below code will remove anything that is not a-z, 0-9 or a dot.
$file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $target_file);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

               $this->modele->ajouter_photo_post($target_file,$titre,$description,$id);
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        header("Location: http://maximepocq.com/Indian_Lotus/?module=modif_Acueil&form=confirm");
    }

    public function modif_photo_existant($tab){
        $id_photo=$tab['id_photo'];
        $id_post=$tab['id_post'];
        $url=$tab['url'];
        var_dump("lapin");
        var_dump($id_photo);
        var_dump($id_post);
        $this->modele->modif_photo_existant($id_post,$id_photo);
       header("Location: $url");

    }

    public function mention(){
    $this->vue->mention();
    }
}
