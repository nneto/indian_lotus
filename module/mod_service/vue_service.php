<?php
class VueService extends VueGenerique
{
    public function vue_service($services){
      $this->contenu.="<header>
    <div class=\"header-content\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"simple-title text-white\">Nos services</h2>
                </div>
            </div>
        </div>
    </div>
</header>
<section id=\"title-services\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12\">
                <div class=\"text-center\">
                    <h3>Nous proposons diverses prestations de cours et d'événements que vous pouvez découvrir ici !</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section id=\"all-services\">
    <div class=\"\">
        <div class=\"service\" id=\"service-danse\">
            <h2 class=\"title-fantasy text-white\">Cours de Danse</h2>
            <div class=\"service-link\">
                <a class=\"text-service btn btn-white\" href=\"?module=detail_service&service=Danse\">Découvrir nos cours</a>
            </div>
        </div>
        <div class=\"service\" id=\"service-musique\">
            <h2 class=\"title-fantasy text-white\">Cours de Musique</h2>
            <div class=\"service-link\">
                <a class=\"text-service btn btn-white\" href=\"?module=detail_service&service=Musique\">Découvrir nos cours</a>
            </div>
        </div>
        <div class=\"service\" id=\"service-coursPrives\">
            <h2 class=\"title-fantasy text-white\">Cours privés</h2>
            <div class=\"service-link\">
                <a class=\"text-service btn btn-white\" href=\"?module=detail_service&service=Prive\">Découvrir nos cours privés</a>
            </div>
        </div>
        <div class=\"service\" id=\"service-spectacles\">
            <h2 class=\"title-fantasy text-white\">Nos prestations</h2>
            <div class=\"service-link\">
                <a class=\"text-service btn btn-white\" href=\"?module=detail_service&service=Spectacle\">Découvrir nos prestations</a>
            </div>
        </div>
        <div class=\"service\" id=\"service-planning\">
            <h2 class=\"title-fantasy text-white\">Notre Planning</h2>
            <div class=\"service-link\">
                <a class=\"text-service btn btn-white\" href=\"?module=detail_service&service=Planning\">Découvrir notre planning</a>
            </div>
        </div>
        </section>
";

     
    }

    public function vue_service_posts($posts,$services){

        $cpt=0;
        $this->contenu.="
  <header>
   <div class=\"header-content\" id=\"header-$_GET[service]\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"title-fantasy text-white\">$_GET[service]</h2>
                    
                </div>
            </div>
        </div>
    </div>
</header>
        ";
        foreach ($posts as $post){
        if($cpt%2 == 0){
            $this->contenu.="
    <section  class=\"section-cours\">
    <div class=\"row title-red\">

<div class=\"col-8 title title-red text-right\">
    ";
      
        }else{
                    $this->contenu.="
    <section  class=\"section-cours\">
    <div class=\"row title-orange\">
    <div class='col-4'>&#8203;</div>
      <div class=\"col-8 title title-orange text-left\">
    ";
        }
            $this->contenu.="
            			<h2 style='text-transform: uppercase' class=\"text-white\">$post[titre_post]</h2>
            			<h2 class=\"title-fantasy text-white\">$post[sous_titre]</h2>
            			   </div>
        	   			<div class='col-4'>&#8203;</div>
    						</div>
    						<div class='container'>";
                        if($cpt%2 == 0){
                            $this->contenu.="<div class='bloc-right'>";

                        }else{
                            $this->contenu.="<div class='bloc-left'>";
                        }    
                        $this->contenu.=" <div class= \"img-over\">
                <img src=\"./$post[chemin]\" alt=\"\" />
            </div>
        </div>";
        				if($cpt%2 == 1){
                            $this->contenu.="<div class='bloc-right'>";

                        }else{
                            $this->contenu.="<div class='bloc-left'>";
                        }    
            		$this->contenu.="<div class='wrapper'>
                <div class='text-left'>
                    <p>$post[description_post]</p>
                    </div>
                      <a class='btn btn-color' href='?module=contact'>NOUS REJOINDRE</a>
            </div>
        </div>       

    </div>
</section>
<div class=\"clear\"></div>
            ";
            $cpt++;
        }
$this->contenu.="        <section class=\"page-change\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12 change-content text-center\">
                <h3>Voir nos autres services</h3>
                <div class=\"other-links\">";

                    foreach ($services as $service ) {
                        if($_GET['service'] != $service['titre_Service_Post'] ){
                         $this->contenu.="
                    <h4 class=\"text-black\"><a href=\"?module=detail_service&service=$service[titre_Service_Post]\" class=\"text-black\">$service[titre_Service_Post]</a></h4>
                    "; 
                        }
                          
                    }
                  $this->contenu.="                    
                    
                </div>
                
            </div>
        </div>
    </div>
</section>";

    }

    public function form_ajout_post_service($posts,$photos,$token,$idservice){
         $this->contenu.="
          
          
          <section class='section-modif-accueil'>
        <section class=\"bo-contener\">
    		<div class=\"headPost\">
    	  		<p  class=\"page-titre title-fantasy \">Post de la page $_GET[service]</p>
    	  		
    	  		<button class=\"buttonVisualiser\" onclick='document.location.href=\"?module=detail_service&service=$_GET[service]\"'>Visualiser la page</button>
    ";
    if($_GET['service']!="Planning"){
                $this->contenu.="<button class=\"buttonAjout\" onclick='form_ajout_post()'>Ajouter un nouveau Post</button>";
        
    }
    	  		$this->contenu.="
    	  	</div>";
        
     foreach ($posts as $post){
       
       if($_GET['service']=="Spectacle"){
             $this->contenu.=" 
            <div class=\"post\"  style='height:250px;'>
                <div class=\"form-style-2\">";
        
       }else{
             $this->contenu.=" 
            <div class=\"post\">
                <div class=\"form-style-2\">";
        
       }
               if($_GET['service']!="Spectacle"){
$this->contenu.="<div class=\"form-style-2-heading\"><img src=\"./$post[chemin]\" class=\"img-post\" onclick='modif_photo_post_service($post[id_post])' ></div>";
                }
    				if($_GET['service']!="Planning"){
    			$this->contenu.=" 	<form action='?module=modif_post_Acueil'  method=\"POST\" id='form$post[id_post]'>
    	                <input type='hidden' name='token' value='$token' />
    	                <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=modifier' />
                        <input type='hidden' name='id_post' required value='$post[id_post]'></input>
    					<label for=\"field1\"><span>Titre <span class=\"required\">*</span></span><input type='text' name='titre_post' required value='$post[titre_post]'></input></label>
    					<label for=\"field2\"><span>Sous-titre <span class=\"required\">*</span></span><input type=\"text\" class=\"input-field\" name=\"sous-titre\" value='$post[sous_titre]'/></label>
    					<label for=\"field3\"><span>Description <span class=\"required\">*</span></span><textarea form='form$post[id_post]' name='description' class=\"textarea-field\">$post[description_post]</textarea></label>
    					<input type=\"submit\" class='button-modifier'value=\"Modifier\" />
    				
    				</form>
    					<button class='button-styler ' onclick='supprimerPost($post[id_post],\"$token\",\"$_GET[service]\")'>Supprimer</button> 
    			</div>
    	  	</div>
  	   
  	";}
            }
                $this->contenu.="
           
<div id=\"myModal\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
  <span class=\"close\">&times;</span>
  <h1 class='title-fantasy text-white'>Ajouter Une Image</h1>
    
  <form method='post' action='?module=ajout_Photo_post' id='form' enctype=\"multipart/form-data\">

                      <input type='hidden' name='token' value='$token'/>
                      <input type='hidden' name='id_post' class='input-hidden-id-post' value='0'/>
                                      <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=confirm' />
                      <input type='file' class='input-file-modal' name='photo' accept='.jpg, .jpeg, .png'/>
                      
                      <label class='text-white'>Titre Photo
                          <input type='text' name='titre_photo'></input>
                      </label>
                      <br/>
                      <label class='text-white' >Description
                          <textarea name='description' form='form'></textarea>
                      </label>
                      <input type='submit' value='valider'>
                </form>";
                if(!empty($posts)){
                             foreach ($photos as $photo) {
                $this->contenu.="
                    <img src=\" ./$photo[chemin]\" style=' width:200px ' onclick='modif_photo_post($photo[id_photo],$post[id_post],\"$_GET[service]\")'>";
            }
   
                }    
   
             $this->contenu.="
                
                <form method='post' class='form_modif_photo_existant' action='?module=modif_photo_existant'>
                <input type='hidden' name='id_post' class='input-hidden-id-post' value='0'/>
                <input type='hidden' name='id_photo' class='input-hidden-id-photo' value='0'/>
                <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=modifier' />
                </form>       
                
                </div>
                
                </div>";

     

        $this->contenu.="<div id=\"myModal3\" class=\"modal\" >
                <!-- Modal content -->
                <div class=\"modal-content\" style='background-color: #1f1f1f'>
                    <span class=\"close close3\">&times;</span>
              	
              
                    <section class=\"bo-popup\">
	  	                <div class=\"post-pop\">
	  		            <h2 class='title-fantasy'>Ajouter un Post</h2>
	  		            <div class=\"form-style-2\">
				<form action=\"?module=ajout_post\" method=\"post\" id='ajout_post' enctype='multipart/form-data'>
				        <input type='hidden' name='id_service' value='$idservice'>
				        s<input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=ajouter' />
				        <label for=\"titre\"><span style='color:white;'>Photo <span class=\"required\">*</span></span><input style='color:white;' type='file' name='photo' ></label>
						<label for=\"titre\"><span style='color:white;'>Titre <span class=\"required\">*</span></span><input type=\"text\" class=\"input-field\" name='titre' value=\"\" /></label>
						<label for=\"sous-titre\"><span style='color:white;'>Sous-Titre <span class=\"required\">*</span></span><input type=\"text\" class=\"input-field\" name='sous-titre' value=\"\" /></label>
					<label for=\"description\"><span style='color:white;'>Description <span class=\"required\">*</span></span><textarea name=\"description\" class=\"textarea-field\"></textarea></label>

					<label><span>&nbsp;</span><input type=\"submit\" value=\"Ajouter\" /></label>                                     
                   
                 </form>        
                 ";
        $this->contenu.='</section> </section></section><div class="clear"></div><div class="clear">';
         if(isset($_GET['form'])){
            $this->contenu.="<script>alert('Post $_GET[form]');</script>";
        }
    }
 
 public function vue_service_posts_spectacle($post,$service){
        $this->contenu.="
  <header>
   <div class=\"header-content\" id=\"header-spectacle\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"title-fantasy text-white\">$_GET[service]</h2>
                    
                </div>
            </div>
        </div>
    </div>
</header>
        ";

        $this->contenu.="
<section id=\"spectacles\" class=\"wrapper style3 section-cours\">
    <div class=\"container\">
        <h2 class=\"text-center text-black\">Événements</h2>
        <div class=\"bookmark bookmark-center\"></div>
    </div>
    <p>Indian LOTUS est une association spécialisée dans les danses indiennes, classiques et bollywood. La troupe est composée de plusieurs danseuses et danseurs dynamiques et talentueux, et se démarque par ses prestations à la fois raffinées et festives. Elle vous propose des prestations et spectacles de danse pour tous vos évènements (inaugurations, festivals, cocktails, réceptions, anniversaires et mariages, casinos, évènementiel à thème etc) et de nombreuses animations complémentaires afin de vous transporter en Inde !</p>
    <div class=\"text-center\">
        <a href=\"contact.php\" class=\"btn btn-color \">NOUS CONTACTER</a>
    </div>
</section>
";
$this->contenu.="        <section class=\"page-change\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12 change-content text-center\">
                <h3>Voir nos autres services</h3>
                <div class=\"other-links\">";

                    foreach ($services as $service ) {
                        if($_GET['service'] != $service['titre_Service_Post'] ){
                         $this->contenu.="
                    <h4 class=\"text-black\"><a href=\"?module=detail_service&service=$service[titre_Service_Post]\" class=\"text-black\">$service[titre_Service_Post]</a></h4>
                    "; 
                        }
                          
                    }
                  $this->contenu.="                    
                    
                </div>
                
            </div>
        </div>
    </div>";
 }
public function vue_service_posts_planning($post,$services){
        $this->contenu.="
  <header>
   <div class=\"header-content\" id=\"header-planning\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"title-fantasy text-white\">$_GET[service]</h2>
                    
                </div>
            </div>
        </div>
    </div>
</header>
        ";
        foreach ($post as $p ) {
            if($p['nom_post'] == 'planning'){
                $this->contenu.="<img style='margin:0;' src='./$p[chemin]'></img>";
            }
            
        }
        $this->contenu.="
<section id=\"spectacles\" class=\"wrapper style3 section-cours\">
    
</section>
";

$this->contenu.="        <section class=\"page-change\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12 change-content text-center\">
                <h3>Voir nos autres services</h3>
                <div class=\"other-links\">";

                    foreach ($services as $service ) {
                        if($_GET['service'] != $service['titre_Service_Post'] ){
                         $this->contenu.="
                    <h4 class=\"text-black\"><a href=\"?module=detail_service&service=$service[titre_Service_Post]\" class=\"text-black\">$service[titre_Service_Post]</a></h4>
                    "; 
                        }
                          
                    }
                  $this->contenu.="                    
                    
                </div>
                
            </div>
        </div>
    </div>";
 }
    public function vue_service_posts_prive($post,$services){
        $this->contenu.='   <header>
   <div class="header-content" id="header-prive" >
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h2 class="title-fantasy text-white">Cours Privés</h2>
                    
                </div>
            </div>
        </div>
    </div>
</header> 
<section id="cours-prives" class="wrapper style3 section-cours">
    <div class="container">
        <h2 class="text-center text-black">Nous proposons des cours particuliers / à domicile</h2>
        <div class="bookmark bookmark-center"></div>
    </div>
    <p>Vous souhaitez apprendre sans prendre de cours en groupe, vous avez raté trop de cours et voulez rattraper le retard, vous êtes en situation de handicap, vous voulez vous perfectionner de manière fine et précise, vous avez raté la rentrée de plus d’un mois, alors les cours particuliers sont pour vous.</p>
    <p>Les cours sont donnés par un professeur diplômé en fonction de vos disponibilités soit dans la salle d’entraînement soit chez vous. Les suggestions et demandes sont prises en compte et s’adaptent pendant et au fil des séances. Nous vous expliquons le pourquoi physique et aussi psychologique des mouvements, des blocages éventuels que vous auriez, etc.</p>

    <div class="row">
        <div class="tarif-info">
            <div class="flex flex-6">
                <div class="col">
                    <img src="include/img/placeholder-on-map.svg">
                    <h4>Tarif</h4>
                    <div class="text-tarif">20/25€ l\'heure par personne</div>
                    <div class="legend">(Tarifé selon les déplacements du prof)</div>
                </div>
                <div class="col">
                    <img src="include/img/telephone.svg">
                    <h4>Temps</h4>
                    <p class="text-center">1 heure</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row"><div class="more-tarif-info">Pour les cours de groupe le tarif est de <span class="bolder-text">10€ par personne / par mois</span> et le <span class="bolder-text">forfait «Indian pack» à 90€ par personne/pour l’année.</span></div>
    </div>
    <div class="text-center">
        <a href="?module=contact" class="btn btn-color">NOUS CONTACTER</a>
    </div>
</section>

';
$this->contenu.="        <section class=\"page-change\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-12 change-content text-center\">
                <h3>Voir nos autres services</h3>
                <div class=\"other-links\">";

                    foreach ($services as $service ) {
                        if($_GET['service'] != $service['titre_Service_Post'] ){
                         $this->contenu.="
                    <h4 class=\"text-black\"><a href=\"?module=detail_service&service=$service[titre_Service_Post]\" class=\"text-black\">$service[titre_Service_Post]</a></h4>
                    "; 
                        }
                          
                    }
                  $this->contenu.="                    
                    
                </div>
                
            </div>
        </div>
    </div>";
}
}