<?php

include_once('controleur_service.php');
class ModService extends ModuleGenerique
{
    public function module_service(){
        $this->controleur= new ControleurService();
        $this->controleur->service();
    }

    public function module_post_service(){
            if(isset($_GET['service'])){
                $this->controleur= new ControleurService();
                $this->controleur->service_post();
        }
        
    }

    public function form_ajout_post_service(){        
        if($this->isAdmin()){
            if(isset($_GET['service'])){
                $this->controleur= new ControleurService();
                $this->controleur->form_ajout_post_service();
            }    
        }
        
    }

    public function module_modif_post($tab){
        if($this->isAdmin()){
            if(isset($tab['id_post']) && isset($tab['titre_post']) && isset($tab['sous_titre']) && isset($tab['description'])){
                $this->controleur = new ControleurService();
                $this->controleur->controlleur_modif_post($tab);
            }else{
            }
        }
    }

    public function module_suppr_post($tab){
        if($this->isAdmin()){
            if(isset($tab['token']) && isset($tab['id'])){
                $this->controleur = new ControleurService();
                $this->controleur->controlleur_suppr_post($tab);
            }    
        }
    }

    public function module_ajout_post($tab){
        if($this->isAdmin()){
            if(isset($tab['id_service']) && isset($_FILES['photo']) && isset($tab['titre']) && isset($tab['sous-titre']) && isset($tab['description']) ){
                $this->controleur = new ControleurService();
                $this->controleur->controlleur_ajout_post($tab);
            }    
        }
    }
}
