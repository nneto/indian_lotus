<?php
ini_set("session.lifetime",3600);

require_once('vue_service.php');
require_once ('modele_service.php');
class ControleurService extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueService();
        $this->modele=new ModeleService();
    }

    public function service(){
        $services=$this->modele->getServicePost();
        
        $this->vue->vue_service($services);
    }

    public function service_post(){
        $service=$_GET['service'];
        $posts=$this->modele->getPostServicePost($service);
        $s=$this->modele->getServices();
if($_GET['service']=="Spectacle"){
    $this->vue->vue_service_posts_spectacle($posts,$s);
}else if($_GET['service']=="Planning"){
    $this->vue->vue_service_posts_planning($posts,$s);
}else if($_GET['service']=="Prive"){
$this->vue->vue_service_posts_prive($posts,$s);    
}else{
    $this->vue->vue_service_posts($posts,$s);
}
    }
    public function form_ajout_post_service(){
        $service=$_GET['service'];
        $posts=$this->modele->getPostServicePost($service);
        $photos=$this->modele->getPhoto();
        $token=$this->modele->createToken();
        $idService=$this->modele->getIdService($_GET['service']);

        $this->vue->form_ajout_post_service($posts,$photos,$token,$idService['id_Service_Post']);
    }

    public function controlleur_modif_post($tab){
        $id=$tab['id_post'];
        $titre=$tab['titre_post'];
        $sout_titre=$tab['sous_titre'];
        $description=$tab['description'];
        $this->modele->modif_post($id,$titre,$sout_titre,$description);
    }

    public function controlleur_suppr_post($tab){
        $id=$tab['id'];
        $token=$tab['token'];
        $service=$_GET['service'];
        if(!empty($this->modele->getToken($token))){
            $this->modele->suppr_post($id);
        }
          header("Location:?module=form_ajout_post_service&service=$service&form=supprimer");
        
    }

    public function controlleur_ajout_post($tab)
    {

        $id_service = $tab['id_service'];
        $titre = $tab['titre'];
        $sous_titre = $tab['sous-titre'];
        $description = $tab['description'];
        $url=$tab['url'];
        $target_dir = "image/";
        $file_name=preg_replace("/[^a-zA-Z0-9.]/", "", $_FILES["photo"]["name"]);
        $target_file = $target_dir . basename(time().$file_name);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if ($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

                $this->modele->ajout_post($id_service, $titre, $sous_titre, $description,$target_file);

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        header("Location:$url");
    }

}
