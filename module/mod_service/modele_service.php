<?php
class ModeleService extends ModeleGenerique
{
    public function getServicePost(){
        $r=self::$connexion->prepare("select * from service_post inner join photo on service_post.id_photo=photo.id_photo;");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function getPostServicePost($service){
        $r=self::$connexion->prepare("select * from post INNER JOIN service_post post2 ON post.id_Service_Post = post2.id_Service_Post INNER JOIN photo p ON post.id_photo = p.id_photo WHERE post2.titre_Service_Post = ?;");
        $r->execute(array($service));
        return $r->fetchall(PDO::FETCH_ASSOC);

    }

    public function getServices(){
        $r=self::$connexion->prepare("select * from service_post ;");
        $r->execute(array());
        return $r->fetchall(PDO::FETCH_ASSOC);

    }

    public function getPhoto(){
        $r=self::$connexion->prepare("select * from photo");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function modif_post($id,$titre,$sous_titre,$description){
        $r=self::$connexion->prepare("update post set titre_post=? , sous_titre=?,description_post =? where id_post=?");
        $s=$r->execute(array($titre,$sous_titre,$description,$id));
    }

    public function suppr_post($id){        
        $r=self::$connexion->prepare("delete from post where id_post=?");
        $s=$r->execute(array($id));
    }

    public function ajout_post($id_service,$titre,$sous_titre,$description,$chemin){
        $r=self::$connexion->prepare("insert into photo VALUES (DEFAULT ,?,?,?,?)");
        $r->execute(array($chemin,$titre,$description,time()));
        $id_photo=self::$connexion->lastInsertId();
        $r=self::$connexion->prepare("insert into post VALUES (DEFAULT ,?,?,?,?,?,?,?,?)");
        $a=$r->execute(array($titre,$sous_titre,$description,"Nous Rejoindre",$id_service,$id_photo,time(),''));

    }
    
    public function getIdService($service){
        $r=self::$connexion->prepare("select * from service_post where titre_Service_Post=?");
        $r->execute(array($service));
        return $r->fetch(PDO::FETCH_ASSOC);
    }
}
