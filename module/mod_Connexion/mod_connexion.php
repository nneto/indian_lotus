<?php

include_once('controleur_connexion.php');
class ModConnexion extends ModuleGenerique
{
    public function module_connexion($tab){

        $this->controleur=new ControleurConnexion();
        $this->controleur->controleur_connexion($tab);
    }
    
    public function formulaireConnexion(){
        $this->controleur=new ControleurConnexion();
            
        if($this->isAdmin()){
                 $this->controleur->accueil_admin();
        }else{
            $this->controleur->controleur_formulaire_connexion();
        }
        
    }
    
    public function connexion(){
        if(isset($_POST['token']) &&  isset($_POST['login']) && isset($_POST['mdp'])){
            $this->controleur=new ControleurConnexion();
            $this->controleur->connexion($_POST);
        }
    }
    
    public function deconnexion(){
        if($this->isAdmin()){
            $this->controleur=new ControleurConnexion();
            $this->controleur->deconnexion();
        }
    }
    
    public function form_mdp(){
        $this->controleur=new ControleurConnexion();
        if($this->isAdmin()){
            $this->controleur->form_mdp();
        }else{
            
        }
    }
    
    public function change_mdp(){
        $this->controleur=new ControleurConnexion();
        if($this->isAdmin()){
            if(isset($_POST['token']) && isset($_POST['old_psw'])  && isset($_POST['new_password']) && isset($_POST['confirm_password'])  ){
                $this->controleur->change_mdp($_POST);
            }
            
        }else{
            
        }
    }
}
