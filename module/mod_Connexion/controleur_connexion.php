<?php

require_once('model_connexion.php');
require_once('vue_connexion.php');
class ControleurConnexion extends ControleurGenerique
{
    public function __construct()
    {
        $this->vue=new VueConnexion();
        $this->modele=new ModelConnexion();
    }

    public function controleur_formulaire_connexion(){
        $token=$this->modele->createToken();
        $this->vue->formulaireConnexion($token);
    }
    
    public function deconnexion(){
        unset( $_SESSION['email']);
        unset( $_SESSION['pseudo']);
        unset( $_SESSION['idcompte']);
        header('Location:index.php?module=Accueil');
    }
    public function accueil_admin(){
        
        $this->vue->accueil_admin($this->modele->getService());
    }
    public function connexion($tab){
        $r=$this->modele->getToken($tab['token']);
        if(empty($r)){
            $this->modele->effacerToken($tab['token']);
        }else {
            $this->modele->effacerToken($tab['token']);
            $compte = $this->modele->getCompte($tab);
            if ($compte == NULL) {
                $this->modele->effacerToken($tab['token']);
            }else{
                $this->modele->effacerToken($tab['token']);
                $_SESSION['email']=$compte[0]['email_user'];
                $_SESSION['pseudo']=$compte[0]['pseudo'];
                $_SESSION['idcompte']=$compte[0]['id_user'];

            }
        }
    }
    
    public function form_mdp(){
        $token=$this->modele->createToken();
        $this->vue->form_mdp($token);
        
        
    }
    
    public function change_mdp($tab){
    var_dump($tab);
        $token=$tab['token'];
        $old_psw=$tab['old_psw'];
        $new_psw=$tab['new_password'];
        $confirm_psw=$tab['confirm_password'];
        
        if(!empty($this->modele->getToken($token))){
            if($old_psw != $new_psw && $new_psw== $confirm_psw){
                var_dump("no_err");
               $this->modele->changeMdp($new_psw);
               $this->deconnexion();      
            }else{
                var_dump("err");
            }
              
        }
    
    }
    
}
