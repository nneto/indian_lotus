<?php

require_once('vue_equipe.php');
require_once('modele_equipe.php');
class ControleurEquipe extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueEquipe();
        $this->modele=new ModeleEquipe();
    }

    public function affiche_formulaire(){
       $token= $this->modele->createToken();
       $equipe= $this->modele->getEquipe();
       $this->vue->vue_Equipe_formulaire($token,$equipe);
    }

    public function modif_equipe($tab){
		$idPersonne=$tab['id_personne'];
		$nom=$tab['nom'];
		$prenom=$tab['prenom'];
	        $status=$tab['status'];
		$description=$tab['description'];
		$this->modele->modifEquipe($idPersonne,$nom,$prenom,$description,$status);

        header('Location:?module=formulaire_equipe&form=modifier');
	}

	public function suppr_equipe($tab){

		if(!empty($this->modele->getToken($tab['token']))){

			$this->modele->suprimer_equipe($tab['id']);
		}
		header('Location:?module=formulaire_equipe&form=supprimer');
	}


	public function ajouter_equipe($tab){
        $token=$tab['token'];
        $id=$tab['id_personne'];
        $nom=$tab['nom'];
        $prenom=$tab['prenom'];
        $description=$tab['description'];
        $status=$tab['statut'];
        $target_dir = "image/";
        $target_file = $target_dir . basename(time()."".$_FILES["photo"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

                $this->modele->ajouter_pesonne($target_file,$nom,$prenom,$description,$status);

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
//        header('Location:?module=formulaire_equipe&form=ajouter');
    }

    public function modif_photo_personne(){
        $id=$_POST['id_personne'];


        $target_dir = "image/";
        $target_file = $target_dir . basename(time()."".$_FILES["photo"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

                $this->modele->modif_photo_pesonne($target_file,$id);

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
        header('?module=formulaire_equipe&form=modifier');
    }
}
