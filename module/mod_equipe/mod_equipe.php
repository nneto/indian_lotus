<?php


include_once('controleur_equipe.php');
class ModEquipe extends ModuleGenerique
{



    public function module_formulaire_equipe(){
        $this->controleur=new ControleurEquipe();
        $this->controleur->affiche_formulaire();
    }

    public function modif_equipe(){
		$this->controleur=new ControleurEquipe();
    	if(isset($_POST['id_personne']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['description'])){
			$this->controleur->modif_equipe($_POST);
		}else{
		}
	}
	public function supprimer_equipe(){
		$this->controleur=new ControleurEquipe();
		
		if(isset($_GET['id']) && isset($_GET['token'])){
			$this->controleur->suppr_equipe($_GET);
		}
	}

	public function ajouter_equipe(){
		$this->controleur=new ControleurEquipe();
		if( isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['description'])){
	           $this->controleur->ajouter_equipe($_POST);        
		}
	}

	public function modif_photo_personne(){
        $this->controleur=new ControleurEquipe();

        if( isset($_POST['id_personne']) && isset($_FILES['photo'])){
            $this->controleur->modif_photo_personne();
        }
    }

}
