<?php

class VueEquipe extends VueGenerique
{
	public function vue_Equipe_formulaire($token,$equipe)
	{

		$this->titre = "Equipe";
		$this->Css = array("<link href=\"\" rel=\"\">");
$this->contenu.="
          <section class='section-modif-accueil'>
        <section class=\"bo-contener\">
    		<div class=\"headPost\">
    	  		<p  class=\"page-titre title-fantasy \">Modifier équipe</p>
    	  		<button class=\"buttonAjout\" onclick=\"form_ajout_equipe()\">Ajouter un nouveau membre</button>
    	  		<button class=\"buttonVisualiser\" onclick='document.location.href=\"?module=propos\"'>Visualiser la page</button>
    	  		
    	  	</div>";
        
    
        foreach ($equipe as $membre){
        $this->contenu.=" 
    	  	<div class=\"post\">
    	  		<div class=\"form-style-2\" >
    				<div class=\"form-style-2-heading\"><img src=\"./$membre[cheminImage]\" class=\"img-post\" onclick='afficheModalPhotoPersonne($membre[id_personne],1)' ></div>
    					<form action=\"?module=modif_personne\" method=\"post\" id='from$membre[id_personne]'>   
    	                <input style='margin : 0;' type='hidden' name='token' value='$token' />
    	  							<input style='margin : 0;'  type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=confirm' />
                       <input style='margin : 0;' type='hidden' name='id_personne' value='$membre[id_personne]'>
    			<label for=\"field1\"><span>Nom <span class=\"required\">*</span></span><input style='margin : 0;' type=\"text\" class=\"input-field\" name=\"nom\" value=\"$membre[nom]\" /></label>
					<label for=\"field2\"><span>Prenom <span class=\"required\">*</span></span><input style='margin : 0;' type=\"text\" class=\"input-field\" name=\"prenom\" value=\"$membre[prenom]\" /></label>
					<label for=\"field3\"><span>Statut <span class=\"required\">*</span></span><input style='margin : 0;' type=\"text\" class=\"input-field\" name=\"status\" value=\"$membre[status]\" /></label>
          <label for=\"field4\"><span>Description <span class=\"required\">*</span></span><textarea style='margin : 0;' name=\"description\" class=\"textarea-field\" form='from$membre[id_personne]' value='$membre[description]'>$membre[description]</textarea></label>
    				<input type=\"submit\" class='button-modifier'value=\"Modifier\" />
    				</form>
    								<button class='button-styler' onclick='supprimerPersonne($membre[id_personne],\"$token\")'>Supprimer</button> 
    			</div>
    	  	</div>";
            }
            
$this->contenu .="<div id=\"myModal4\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
    <span class=\"close close4\">&times;</span>
  <form method='post' action='?module=ajouter_equipe' id='form' style='color: white' enctype=\"multipart/form-data\">

                      <input type='hidden' name='token' value='$token'/>
                                          
                      <input type='file' name='photo' accept='.jpg, .jpeg, .png'/>
                      <label>Nom<input type='text' name='nom' required/></label>
                      <label>Prenom<input type='text' name='prenom' required/></label>
                      <label>Statut<input type='text' name='statut' required/></label>
                      <label>Description</label>
                      <textarea name='description' form='form'></textarea>
                      <input type='submit'  style='background-color:#FF8500;' value='valider'>
                </form>";



        $this->contenu .="
        </section>
  	        <div id=\"myModal5\" class=\"modal\" >
                <!-- Modal content -->
                <div class=\"modal-content2\" style='background-color: #1f1f1f; '>
                    <span class=\"close close4\">&times;</span>
                    <section class=\"bo-popup\">
	  	                <div class=\"post-pop\">
	  		            <h2 class='title-fantasy'>Image</h2>
	  		            <div class=\"form-style-2\">
				
				<form action=\"?module=modif_photo_personne\" method=\"post\" enctype=\"multipart/form-data\">
				    <input type='hidden' name='id_personne' class='id_post' value='0'>
					<label for=\"titre\"><span style='color:white;'>Photo <span class=\"required\">*</span></span><input type='file' name='photo' ></label>
					<label><span>&nbsp;</span><input type=\"submit\"  value=\"Ajouter\" /></label>
				</form>				
			</div>
		";
        $this->contenu.='<div class="clear"></div></section> </section>';
        $this->contenu.='<div class="clear"></div>';
         if(isset($_GET['form']) && $_GET['form']=='ajouter'){
            $this->contenu.="<script>alert('Membre ajouter');</script>";
        }elseif(isset($_GET['form']) && $_GET['form']=='supprimer'){
          $this->contenu.="<script>alert('Membre supprimer');</script>";
        }else if (isset($_GET['form']) && $_GET['form']=='modifier'){
          $this->contenu.="<script>alert('Membre modifier');</script>";
        }
}

}
