<?php

class ModeleEquipe extends ModeleGenerique
{
    public function getEquipe(){
        try{
            $requeteCompte='select * from equipe';
            $requete=self::$connexion->prepare($requeteCompte);
            $requete->execute(array());
            $tab2=$requete->FetchAll(PDO::FETCH_ASSOC);
            return $tab2;
        }catch (PDOException $p){
        }
    }

    public function modifEquipe($id,$nom,$prenom,$description,$status){
    
		$r=self::$connexion->prepare("update equipe set nom=? ,prenom=? ,description=?,status=? where id_personne=?");
		$s=$r->execute(array($nom,$prenom,$description,$status,$id));
	}

    public function ajouter_pesonne($chemin,$nom,$prenom,$description,$status){
        $r=self::$connexion->prepare("insert into equipe VALUES (DEFAULT ,?,?,?,?,?)");
        $s=$r->execute(array($nom,$prenom,$description,$chemin,$status));
    }

	public function ajouter_photo_personne($chemin,$nom,$prenom,$description,$id){
		$r=self::$connexion->prepare("insert into photo VALUES (DEFAULT ,?,?,?,?)");
		$r->execute(array($chemin,$nom.$prenom,$description,time()));
		$id_photo=self::$connexion->lastInsertId();
		$r=self::$connexion->prepare("update personne set id_photo=? where id_personne=?");
		$a=$r->execute(array($id_photo,$id));
	}

	public function suprimer_equipe($id){
		$r=self::$connexion->prepare("delete from equipe WHERE id_personne=?" );
		$s=$r->execute(array($id));
	}

	public function modif_photo_pesonne($chemin,$id){
        $r=self::$connexion->prepare("update equipe set cheminImage=? where id_personne=?" );
        $s=$r->execute(array($chemin,$id));
    }
}
