<?php


include_once('controleur_contact.php');
class ModContact extends ModuleGenerique
{
    public function __construct()
    {
        $this->controleur= new ControleurContact();
    }

    public function module_contact(){

        if(isset($_GET['envoyer']) && isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['mail'])   ){
            $this->controleur->controleur_contact_envoyer();
        }else{
            $this->controleur->controleur_contact();
        }

   }
}