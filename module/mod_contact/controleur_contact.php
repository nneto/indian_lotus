<?php

require_once('vue_contact.php');
require_once ('modele_contact.php');
class ControleurContact extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueContact();
        $this->modele=new ModeleContact();
    }

    public function controleur_contact(){
            $this->vue->vue_Contact();
    }

    public function controleur_contact_envoyer(){
        if($_GET['envoyer']==1){

            $this->modele->envoyerMail1($_POST['nom'],$_POST['prenom'],$_POST['tel'],$_POST['mail'],$_POST['message']);

        }else if ($_GET['envoyer']==2){

            $this->modele->envoyerMail2($_POST['nom'],$_POST['datenaissance'],$_POST['prenom'],$_POST['tel'],$_POST['mail'],$_POST['message'],$_POST['adresse'],$_POST['cp'],$_POST['ville'],$_POST['$nom2'],$_POST['$prenom2'],$_POST['$tel2'],$_POST['$mail2']);

        }else{
            $this->vue->vue_Contact();
        }

    }

}
