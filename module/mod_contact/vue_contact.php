<?php
class VueContact extends VueGenerique
{
    public function vue_Contact()
    {

        $this->titre = "Contact";
        $this->Css = array("<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css\">","<script src=\"https://code.jquery.com/jquery-1.12.4.js\"></script>","<script src=\"https://code.jquery.com/ui/1.12.1/jquery-ui.js\"></script>");
        $this->contenu .= "
        <header id=\"header-contact\">
    <?php require 'inc/nav.inc.php';?>
    <div class=\"header-content\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"simple-title text-white\">NOUS REJOINDRE</h2>
                </div>
            </div>
        </div>
    </div>

</header>

<section id=\"contact\">
    <div class=\"bg-fantasy\">
        <div class=\"container\">
            <h2 class=\"title-fantasy\">Contact</h2>
            <div class=\"bookmark\"></div>
        </div>
    </div>
    <div class=\"container\">
        <div class=\"contact-text\">
            <p class=\"description-text text-white\">Vous souhaitez plus de renseignements ? Voulez un devis, ou avez des questions sur notre association ? N’hésitez plus : CONTACTEZ-NOUS !</p>
        </div>
        <div class=\"row\">
            <div class=\"form-wrapper\">
                <form id=\"form-contact\" class=\"clearfix\" method=\"post\" action=\"?module=contact&envoyer=1\">
                    <div class=\"row-form\">
                        <label class=\"demande-form\" for=\"type-demande\">Type de demande</label>
                        <select id=\"type-demande\" name=\"type-demande\">
                        <option selected disabled>Type de demande</option>
						<option>Demande d'informations</option>
						<option>Demande de devis</option>
					</select>
                    </div>
                    <div class=\"row-form\">
                        <label for=\"nom\">Nom</label>
                        <input type=\"text\" id=\"nom\" class=\"half-input\" name=\"nom\" placeholder=\"Nom\" required>

                        <label for=\"prenom\">Prénom</label>
                        <input type=\"text\" id=\"prenom\" class=\"half-input\" name=\"prenom\" placeholder=\"Prénom\" required>
                    </div>
                    <div class=\"row-form\">
                        <label for=\"tel\">Téléphone</label>
                        <input type=\"text\" id=\"tel\" class=\"half-input\" name=\"tel\" placeholder=\"Téléphone\" required>
                        <label for=\"mail\">E-mail</label>
                        <input type=\"text\" id=\"mail\" class=\"half-input\" name=\"mail\" placeholder=\"E-mail\" required>
                    </div>
                    <div class=\"row-form\">
                        <label for=\"message\">Votre message</label>
                        <textarea id=\"message\" name=\"message\" placeholder=\"Message\"></textarea>
                    </div>
                    <div class=\"row-form\">
                        <input type=\"submit\" class=\"submit-white\" name=\"submit-contact\" value=\"Envoyer\" required>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
";

        $this->contenu.="<section id=\"demande-adhesion\">
    <div class=\"container\">
        <h2 class=\"title-fantasy\">Demande d'adhésion</h2>
        <div class=\"bookmark\"></div>
        <div class=\"adhesion-text\">
            <h3>VOUS SOUHAITEZ ADHÉRER ?</h3>
            <p class=\"description-text text-black\">Si vous souhaitez vous inscrire dans nos cours de danse, de chant, ou de culture indienne, nous vous prions de nous retourner le formulaire d’adhésion ci-dessous, avec les pièces nécessaires à votre inscription et rendez-vous au prochain cours !</p>
        </div>
        <div class=\"form-wrapper\">
            <form id=\"form-adhesion\" class=\"clearfix\" method=\"post\" action=\"?module=contact&envoyer=2\">
                <div class=\"row-form\">
                    <label for=\"nom\">Nom de l'élève :</label>
                    <input type=\"text\" id=\"nom\" class=\"half-input\" name=\"nom\" placeholder=\"Nom\" required>
                    <label for=\"prenom\">Prénom de l'élève :</label>
                    <input type=\"text\" id=\"prenom\" class=\"half-input\" name=\"prenom\" placeholder=\"Prénom\" required>
                </div>
                <div class=\"row-form\">
                    <label for=\"date-naissance\">Date de naissance :</label>
                    <input type=\"text\" id=\"date-naissance\" class=\"half-input\" name=\"datenaissance\" placeholder=\"Date\"required>
                    <label for=\"tel\">Téléphone</label>
                    <input type=\"text\" id=\"tel\" class=\"half-input\" name=\"tel\" placeholder=\"N° Téléphone\" required>
                </div>
                <div class=\"row-form\">
                    <label for=\"mail\">E-mail :</label>
                    <input type=\"text\" id=\"mail\" class=\"half-input\" name=\"mail\" placeholder=\"E-mail\" required>
                    <label for=\"adress\">Adresse :</label>
                    <input type=\"text\" id=\"adresse\" class=\"half-input\" name=\"adresse\" placeholder=\"Adresse\" required>
                </div>
                <div class=\"row-form\">
                    <label for=\"cp\">Code Postal :</label>
                    <input type=\"text\" id=\"cp\" class=\"half-input\" name=\"cp\" placeholder=\"Code postal\" required>
                    <label for=\"ville\">Ville :</label>
                    <input type=\"text\" id=\"ville\" class=\"half-input\" name=\"ville\" placeholder=\"Ville\" required>
                </div>
                <div class=\"clear\"></div>
                <p class=\"text-black\">Responsable légal (Enfant de moins de 18 ans)</p>
                <div class=\"row-form\">
                    <label for=\"responsable-nom\">Nom du responsable</label>
                    <input type=\"text\" id=\"responsable-nom\" class=\"half-input\" name=\"responsable-nom\" placeholder=\"Nom du responsable légal\" required>
                    <label for=\"responsable-prenom\">Prénom du responsable</label>
                    <input type=\"text\" id=\"responsable-prenom\" class=\"half-input\" name=\"responsable-prenom\" placeholder=\"Prénom du responsable légal\" required>
                </div>
                <div class=\"row-form\">
                    <label for=\"responsable-tel\">N° téléphone / Domicile / Professionnel</label>
                    <input type=\"text\" id=\"responsable-tel\" class=\"half-input\" name=\"responsable-tel\" placeholder=\"N° Téléphone\" required>
                    <label for=\"responsable-mail\">Email du responsable</label>
                    <input type=\"text\" id=\"responsable-mail\" class=\"half-input\" name=\"responsable-mail\" placeholder=\"E-mail du responsable\" required>
                </div>
                <div class=\"row-form\">
                    <input type=\"submit\" name=\"submit-adhesion\" value=\"Envoyer\">
                </div>
            </form>
        </div>
    </div>
</section>

<section id=\"infos\">
    <div class=\"container\">
        <h2 class=\"title-fantasy\">Coordonnées</h2>
        <div class=\"bookmark\"></div>
        <div class=\"flex flex-4\">
            <div class=\"col\">
                <div class=\"icon fit\">
                    <img src=\"include/img/placeholder-on-map.svg\">
                </div>
                <p class=\"bigger-text text-center\">94 rue jean Jaurès, <br> 93130 Noisy le Sec France</p>
            </div>
            <div class=\"col\">
                <div class=\"icon fit\">
                    <img src=\"include/img/telephone.svg\">
                </div>
                <p class=\"bigger-text text-center\">07 67 81 07 35</p>
            </div>
            <div class=\"col\">
                <div class=\"icon fit\">
                    <img src=\"include/img/email-envelope.svg\">
                </div>
                <p class=\"bigger-text text-center\">contact@indianlotus.com</p>
            </div>
        </div>
    </div>
</section>
<div class=\"map\">
    <iframe src=\"https://www.google.com/maps/embed/v1/place?q=place_id:ChIJPwaDXdVs5kcRjHDb5fh9Pxc&key=AIzaSyDTxSJd5UKhug7HIUH2NeyTRuhmwLCjdLU\" allowfullscreen></iframe>
</div>
";

    }
}
