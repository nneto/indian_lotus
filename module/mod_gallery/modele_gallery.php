<?php

class ModeleGallery extends ModeleGenerique
{
    public function getPhotos(){
        $r=self::$connexion->prepare("select * from photo ORDER BY datePhoto DESC ");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function ajouter_photo($chemin,$titre,$description){
        $r=self::$connexion->prepare("insert into photo VALUES (DEFAULT ,?,?,?,?)");
        return $r->execute(array($chemin,$titre,$description,time()));
    }


    public function ajouter_video($url,$titre,$description){
        $r=self::$connexion->prepare("insert into video VALUES (DEFAULT ,?,?,?,?)");
        return $r->execute(array($titre,$description,$url,time()));
    }

    public function modif_photo($id,$titre,$description){
        $r=self::$connexion->prepare("update photo set titre_photo=? , description_image=? where id_photo=? ");
    }

    public function modif_video($id,$titre,$description){
        $r=self::$connexion->prepare("update video set titre_video=? , description_video=? where id_Video=? ");
    }

    public function getVideo(){
        $r=self::$connexion->prepare("select * from video ORDER BY date_video DESC ");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function photoInPost(){
        $r=self::$connexion->prepare("select photo.id_photo from photo INNER JOIN post p ON photo.id_photo = p.id_photo");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }


    public function postTag(){
        $r=self::$connexion->prepare("select * from photo_relation_tag INNER JOIN tag  ON photo_relation_tag.id_tag = tag.id_tag");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }

    public function getTag(){
        $r=self::$connexion->prepare("select * from tag");
        $r->execute();
        return $r->fetchall(PDO::FETCH_ASSOC);
    }
}
