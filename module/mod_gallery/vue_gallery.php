<?php

class VueGallery extends VueGenerique
{
    public function vue_Gallery($photos,$videos,$photoPostPhoto,$tagPhotos,$tags)
    {
        $tabTag=array();
        if (isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte'])){
            $isAdmin = 1;    
        }else{
            $isAdmin = 0;    
        }
        
        $this->contenu .="<header id=\"header-gallery\">
    
    <div class=\"header-content\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-12 text-center\">
                    <h2 class=\"simple-title text-white\">Galerie</h2>
                </div>
            </div>
        </div>
    </div>
</header>";
        
        $this->contenu .="
        <section id=\"photos-gallery\">
    <div class=\"container\">
        <h2 class=\"title-fantasy text-white\">Photos</h2>
        <div class=\"bookmark\"></div>
    </div>
    <div class=\"button-group filters-button-group\">
        <span class=\"filtertitle\">Filtres :</span>
        <button class=\"filter\" onclick=\"document.location.href='?module=Gallery'\" >Tous</button>
        ";
        foreach ($tags as $tag){
            $this->contenu .="
             <button class=\"filter\" onclick=filterPhoto($tag[id_tag],$isAdmin) >$tag[nom_tag]</button>
            ";
        }
        $this->contenu .="</div>";
        
            $this->contenu .="<div class='masonry'>";
        foreach ($photos as $photo){
            $estPresentDansPost= false;
            $this->contenu .="<div class='item'><img class='image-flex' style='width: 400px;height: auto;' src=\"./$photo[chemin] \" alt=\"lapin\"></img> ";
         
            foreach ($photoPostPhoto as $p){
                if($p['id_photo'] == $photo['id_photo']  ){
                    $estPresentDansPost=true;
                }
            }
                         $this->contenu .= "</div>";
        }
            
            $this->contenu .= "</div>";
            $this->contenu.="
    
            </div>
            ";        
    $this->contenu .="<section id=\"videos-gallery\">
    <div class=\"container\">
        <h2 class=\"title-fantasy text-white\">Videos</h2>
        <div class=\"bookmark\"></div>
    </div>
    
    <div class=\"clear\"></div>
    <div class=\"\">
        <div class=\"masonry2\">
    ";
foreach($videos as $video){
    
    $this->contenu .="<div class=\"video-gallery\">
                <iframe width=\"500\" height=\"300\" src=\"$video[url_video]\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
    

    
    $this->contenu .="</div>";
}

$this->contenu .="</div>";
$this->contenu .="</div></section></section>";
    if($isAdmin){
$this->contenu .="
<div id=\"myModal\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
        <p>Voulez vous vraiment supprimer cet �lement</p>
        <input class='idPhoto' type='hidden' value='0'>
        <input class='debug' type='hidden' value='1'>
        <button onclick='supprimerPhotoClick()'>Valider</button>
        <button class='close'>Annuler</button>
    </div>
    </div>
    <div id=\"myModal2\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
        <p>Voulez vous vraiment supprimer cet �lement</p>
        <input class='idVideo' type='hidden' value='0'>
        <input class='debug' type='hidden' value='1'>
        <button onclick='supprimerVideoClick()'>Valider</button>
        <button class='close'>Annuler</button>
    </div>
    </div>

";
}
}


public function vue_ajouter_gallery($token){
    
    if(isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte'])){
        $this->contenu .="<div style='margin-top: 5em;'>
                <form method='post' action='?module=ajout_Photo' id='form' enctype=\"multipart/form-data\">
                      <input type='hidden' name='token' value='$token'/>
                      <label>Tittre photo</label>
                      <input type='file' name='photo' accept='.jpg, .jpeg, .png'/>
                      <input type='text' name='titre_photo'  />
                      <textarea name='description' form='form'></textarea>
                      <input type='submit' value='Ajouter'>
                </form>
                </div>
                ";
    }
}

public function vue_ajouter_video_gallery($token){
    if(isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte'])){
        $this->contenu .="
        <div style='margin-top: 5em;'>
        <form method='post' action='?module=ajout_Video' id='form' enctype=\"multipart/form-data\">
                      <input type='hidden' name='token' value='$token'/>                      
                      <label>Lien Video</label>
                      <input type='text' name='video'/>
                      <label>Titre Video</label>
                      <input type='text' name='titre_video'/>
                      <textarea name='description' form='form'></textarea>
                      <input type='submit' value='Ajouter'>
                </form>
                </div>
                ";


    
        
    }
    
}

public function vue_photo_admin($photos,$photoPostPhoto,$postTag,$tags,$token){


        $tabTag=array();
        if (isset($_SESSION['email']) && isset($_SESSION['pseudo']) && isset($_SESSION['idcompte'])){
            $isAdmin = 1;    
        }else{
            $isAdmin = 0;    
        }


        $this->contenu.=" <section class='section-modif-accueil'>
        <section class=\"bo-contener\">
            <div class=\"headPost\">
                <p  class=\"page-titre title-fantasy \">Modifier Photo</p>
                <button class=\"buttonAjout\" onclick='form_ajout_equipe()'>Ajouter une nouvelle photo</button>
                <button class=\"buttonVisualiser\" onclick=\"document.location.href='?module=Gallery' \">Visualiser la page</button>                
            </div>";
        
    
        foreach ($photos as $photo){
                     $estPresentDansPost= false;

        $this->contenu.=" 
            <div class=\"post\">
                <div class=\"form-style-2\">
                    <div class=\"form-style-2-heading\"><img src=\"./$photo[chemin]\" class=\"img-post\" ></div>
                    <label style='margin-bottom:0'><span style='margin-top:5px'>Ajouter un tag:</span >
                    ";

                $this->contenu .= "<form method='POST' action='?module=ajouterTagPhoto'>";
                $this->contenu .="<input type='hidden'  class='' name='id_photo' value='$photo[id_photo]' name='id'  ></input>";
                $this->contenu .= "<select id='selectTag$photo[id_photo]' name='tag' >";

                foreach ($tags as $tag) {
                    if (!in_array( $tag['id_tag'],$tabTag)) {
                        $this->contenu .= "<option value='$tag[id_tag]'>$tag[nom_tag]</option>";
                    }
                }
                  $this->contenu .= "</select>";
         $this->contenu .= "<button id='buttonAjouterTag$photo[id_photo]' class='button-styler' type='submit' style='width:10em' >Ajouter un tag</button>
                        </form></label>";


                      foreach ($postTag as $tag ){
                    if($tag['id_photo'] == $photo['id_photo'] ){
                        array_push($tabTag,$tag['id_tag']);
                        $this->contenu .="<span >$tag[nom_tag] </span>";   
                    }
                            }

                               $this->contenu.="  <form action=\"?module=modif_photo\" method=\"post\" id='from$photo[id_photo]'>   
                        <input type='hidden' name='token' value='$token' />
                                    <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=modifier' />
                       <input type='hidden' name='id_photo' value='$photo[id_photo]'>
                <label for=\"field1\"><span>Nom <span class=\"required\">*</span></span><input style='margin-left:-30px' type=\"text\" class=\"input-field\" name=\"titre_photo\" value=\"$photo[titre_photo]\" /></label>                    
                    <label for=\"field3\"><span>Description <span class=\"required\">*</span></span><textarea name=\"description_photo\" class=\"textarea-field\" form='from$photo[id_photo]' value='$photo[description_image]'>$photo[description_image]</textarea></label>
                    <input type=\"submit\" class='button-modifier'value=\"Modifier\" />
                    </form>";
                     foreach ($photoPostPhoto as $p){
                if($p['id_photo'] == $photo['id_photo']  ){
                    $estPresentDansPost=true;
                }
            }

                    if(!$estPresentDansPost){
                        $this->contenu .= "<button class='button-styler' onclick='supprimerPhotoClick($photo[id_photo])'>Supprimer</button> ";
                    }
                     $this->contenu .= "                
                </div>
            </div>";
            }
            
        $this->contenu.='<div class="clear"></div></section> </section>';
        $this->contenu.='<div class="clear"></div>';
         if(isset($_GET['form']) && $_GET['form']=='ajouter'){
            $this->contenu.="<script>alert('Photo ajouter');</script>";
        }elseif(isset($_GET['form']) && $_GET['form']=='supprimer'){
          $this->contenu.="<script>alert('Photo supprimer');</script>";
        }else if (isset($_GET['form']) && $_GET['form']=='modifier'){
          $this->contenu.="<script>alert('Photo modifier');</script>";
        }



                $this->contenu .="<div id=\"myModal4\" class=\"modal\">
  <!-- Modal content -->

  <div class=\"modal-content\">

    <span class=\"close close4\">&times;</span>
  <h1 class='title-fantasy'>Ajouter une image</h1>
  <form method='post' action='?module=ajout_Photo' id='form' style='color: white' enctype=\"multipart/form-data\">

                      <input type='hidden' name='token' value='$token'/>                                          
                      <input type='file' name='photo' accept='.jpg, .jpeg, .png'/>
                      <label>Titre photo<input type='text' name='titre_photo' required/></label>                      
                      <label>Description</label>
                      <textarea name='description' form='form'></textarea>
                      <input type='submit'  style='background-color:#FF8500;' value='valider'>
                </form>";
                $this->contenu.="</div></div><div class='clear'></div>";
}



public function vue_video_admin($videos,$token){



        $this->contenu.=" <section class='section-modif-accueil'>
        <section class=\"bo-contener\">
            <div class=\"headPost\">
                <p  class=\"page-titre title-fantasy \">Modifier Video</p>
                <button class=\"buttonAjout\" onclick='form_ajout_equipe()'>Ajouter une nouvelle video</button>
                <button class=\"buttonVisualiser\" onclick=\"document.location.href='?module=Gallery' \">Visualiser la page</button>                
            </div>";
        
    
        foreach ($videos as $video){                     

        $this->contenu.=" 
            <div class=\"post\">
                <div class=\"form-style-2\">
                    <div class=\"form-style-2-heading\"> <iframe width=\"300\" height=\"200\" src=\"$video[url_video]\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe></div>
                    ";

                               $this->contenu.="  <form action=\"?module=modif_video\" method=\"post\" id='from$video[id_Video]'>   
                        <input type='hidden' name='token' value='$token' />
                                    <input type='hidden' name='url' value='http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]&form=modifier' />
                       <input type='hidden' name='id_video' value='$video[id_Video]'>
                <label for=\"field1\"><span>Nom <span class=\"required\">*</span></span><input style='margin-left:-30px' type=\"text\" class=\"input-field\" name=\"titre_video\" value=\"$video[titre_video]\" /></label>                    
                    <label for=\"field3\"><span>Description <span class=\"required\">*</span></span><textarea name=\"description_video\" class=\"textarea-field\" form='from$video[id_Video]' value='$video[description_video]'>$video[description_video]</textarea></label>
                    <input type=\"submit\" class='button-modifier'value=\"Modifier\" />
                    </form>
                                    <button class='button-styler' onclick='supprimerVideoClick($video[id_Video])'>Supprimer</button> 
                </div>
            </div>";
            }
            
        $this->contenu.='<div class="clear"></div></section> </section>';
        $this->contenu.='<div class="clear"></div>';
         if(isset($_GET['form']) && $_GET['form']=='ajouter'){
            $this->contenu.="<script>alert('Video ajouter');</script>";
        }elseif(isset($_GET['form']) && $_GET['form']=='supprimer'){
          $this->contenu.="<script>alert('Video supprimer');</script>";
        }else if (isset($_GET['form']) && $_GET['form']=='modifier'){
          $this->contenu.="<script>alert('Video modifier');</script>";
        }

  $this->contenu .="<div id=\"myModal4\" class=\"modal\">
  <!-- Modal content -->

  <div class=\"modal-content\">

    <span class=\"close close4\">&times;</span>
  <h1 class='title-fantasy'>Ajouter une video</h1>
  <form method='post' action='?module=ajouter_Video' id='form' style='color: white' enctype=\"multipart/form-data\">
  <input type='hidden' name='token' value='$token'/>                      
                      <label>Lien Video</label>
                      <input type='text' name='video'/>
                      <label>Titre Video</label>
                      <input type='text' name='titre_video'/>
                      <textarea name='description' form='form'></textarea>
                      <input type='submit'  style='background-color:#FF8500;' value='valider'>
                </form>";
                $this->contenu.="</div></div><div class='clear'></div>";
}
}