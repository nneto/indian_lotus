<?php


$dns= "mysql:host=localhost;dbname=indianLotus";
$usr= "root";
$psw="root";

$connexion=new PDO($dns,$usr,$psw);
$connexion->exec("SET NAMES 'UTF8'");
$r=$connexion->prepare("delete FROM photo WHERE id_photo=?");
$r->execute(array($_GET['id']));


$r=$connexion->prepare("select * FROM photo ORDER BY datePhoto DESC");
$r->execute(array());
$photos=$r->fetchall(PDO::FETCH_ASSOC);

$r=$connexion->prepare("select * FROM video ORDER BY date_video DESC");
$r->execute(array());
$videos=$r->fetchall(PDO::FETCH_ASSOC);

$r=$connexion->prepare("select photo.id_photo from photo INNER JOIN post p  ON photo.id_photo = p.id_photo ");
$r->execute();
$photoPostPhoto=$r->fetchall(PDO::FETCH_ASSOC);

$r=$connexion->prepare("select * from tag");
$r->execute(array());
$tags=$r->fetchall(PDO::FETCH_ASSOC);

$r=$connexion->prepare("select * from photo_relation_tag INNER JOIN tag  ON photo_relation_tag.id_tag = tag.id_tag");
$r->execute(array());
$tagPhotos=$r->fetchall(PDO::FETCH_ASSOC);
       
        $tabTag=array();
    
            $isAdmin = $_GET['admin'];    
    
        
        $s .="
    <div class=\"container\">
        <h2 class=\"title-fantasy text-white\">Photos</h2>
        <div class=\"bookmark\"></div>
    </div>
    <div class=\"button-group filters-button-group\">
        <span class=\"filtertitle\">Filtres :</span>
        <button class=\"filter\" onclick=\"document.location.href='?module=Gallery'\" >Tous</button>
        ";
        foreach ($tags as $tag){
            $s .="
             <button class=\"filter\" onclick=filterPhoto($tag[id_tag],$isAdmin) >$tag[nom_tag]</button>
            ";
        }
        $s .="</div>";
           if($isAdmin){
            
                $s .="</div><button class='filter' onclick='document.location.href=\"?module=ajout_Gallery\"'>ajouter photo</button>";
                $s .="<button class='filter' onclick='document.location.href=\"?module=ajout_video_Gallery\"'>ajouter video</button>";       
           }
        
            $s .="<div class='masonry'>";
        foreach ($photos as $photo){
            $estPresentDansPost= false;
            $s .="<div class='item'><img class='image-flex' style='width: 400px;height: auto;' src=\"./$photo[chemin] \" alt=\"lapin\"></img> ";
            if($isAdmin){
                $s .="<p class='text-gallery$photo[id_photo]'> titre: $photo[titre_photo]</p> <p class='text-gallery'>description: $photo[description_image] </p>
                <form method='post' action='?module=modif_photo' >";
                $s .="<input type='hidden'  class='input-modif$photo[id_photo]' name='id_photo' value='$photo[id_photo]' name='id'></input>";
                $s .="<input type='text' style='display: none' class='input-modif$photo[id_photo]' name='titre_photo' value='$photo[titre_photo]' name='' ></input>";
                $s .="<input type='text' style='display: none' class='input-modif$photo[id_photo]' name='description_photo' value='$photo[description_image]' name=''></input>";
                $s .="<input type='submit' class='input-modif$photo[id_photo]' style='display: none' value='accepter' ></input>";
                $s .="</form>";
                foreach ($tagPhotos as $tag ){
                    if($tag['id_photo'] == $photo['id_photo'] ){
                        array_push($tabTag,$tag['id_tag']);
                        $s .="<span style='color: red'>$tag[nom_tag] </span>";   
                    }
                }
                $s .= "<button id='buttonTag$photo[id_photo]' onclick='afficheSelect($photo[id_photo])' style='border-radius: 100%'>+</button>";
                $s .= "<form method='POST' action='?module=ajouterTagPhoto'>";
                $s .="<input type='hidden'  class='' name='id_photo' value='$photo[id_photo]' name='id'  ></input>";
                $s .= "<select id='selectTag$photo[id_photo]' name='tag' hidden>";
                foreach ($tags as $tag) {
                    if (!in_array( $tag['id_tag'],$tabTag)) {
                        $s .= "<option value='$tag[id_tag]'>$tag[nom_tag]</option>";
                    }
                }
                  $s .= "</select>";
            $s .= "<button id='buttonAjouterTag$photo[id_photo]' type='submit' style='border-radius: 10%' hidden>ajouter Tag</button>
                        </form>";
            }

            foreach ($photoPostPhoto as $p){
                if($p['id_photo'] == $photo['id_photo']  ){
                    $estPresentDansPost=true;
                }
            }
            
            if($isAdmin){
                if( $estPresentDansPost == false){
                    $s .="<button class='button-no-modif$photo[id_photo]' onclick='afficheModalPhoto($photo[id_photo])' >supprimer</button>";
                }
                $s .="<button class='button-no-modif$photo[id_photo]' onclick='modifierPhoto($photo[id_photo])' >modifier</button>";    
            }
             $s .= "</div>";
        }
            
            $s .= "</div>";
            $s.="
    
            </div>
            ";        
    $s .="<section id=\"videos-gallery\">
    <div class=\"container\">
        <h2 class=\"title-fantasy text-white\">Vidéos</h2>
        <div class=\"bookmark\"></div>
    </div>
    
    <div class=\"clear\"></div>
    <div class=\"\">
        <div class=\"masonry2\">
    ";
foreach($videos as $video){
    
    $s .="<div class=\"\">
                <iframe width=\"500\" height=\"300\" src=\"$video[url_video]\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
    if($isAdmin){
        $s .="<p class='text-video-gallery$video[id_Video]'> titre: $video[titre_video]</p> <p class='text-gallery'>description: $video[description_video] </p>
        <form method='post' action='?module=modif_video' >";
        $s .="<input type='hidden'  class='input-modif$video[id_Video]' name='id_video' value='$video[id_Video]' name='id'  ></input>";
        $s .="<input type='text' style='display: none' class='input-video-modif$video[id_Video]' name='titre_video' value='$video[titre_video]'  ></input>";
        $s .="<input type='text' style='display: none' class='input-video-modif$video[id_Video]' name='description_video' value='$video[description_video]' ></input>";
        $s .="<input type='submit' class='input-video-modif$video[id_Video]' style='display: none' value='accepter' ></input>";
        $s .="</form>";
        $s .="<button class='button-video-no-modiff$video[id_Video]' onclick='afficheModalVideo($video[id_Video],$isAdmin)' >supprimer</button>";
        $s .="<button class='button-video-no-modif$video[id_Video]' onclick='modifierVideo($video[id_Video],$isAdmin)' >modifier</button>";
    }


    
    $s .="</div>";
}

$s .="</div>";
$s .="</div>";
    if($isAdmin){
$s .="
<div id=\"myModal\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
        <p>Voulez vous vraiment supprimer cet élement</p>
        <input class='idPhoto' type='hidden' value='0'>
        <input class='debug' type='hidden' value='1'>
        <button onclick='supprimerPhotoClick()'>Valider</button>
        <button class='close'>Annuler</button>
    </div>
    </div>
    <div id=\"myModal2\" class=\"modal\">
  <!-- Modal content -->
  <div class=\"modal-content\">
        <p>Voulez vous vraiment supprimer cet élement</p>
        <input class='idVideo' type='hidden' value='0'>
        <input class='debug' type='hidden' value='1'>
        <button onclick='supprimerVideoClick()'>Valider</button>
        <button class='close'>Annuler</button>
    </div>
    </div>

";
}

echo $s;
