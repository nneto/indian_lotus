<?php

require_once('vue_gallery.php');
require_once ('modele_gallery.php');
class ControleurGallery extends ControleurGenerique
{

    public function __construct()
    {
        $this->vue=new VueGallery();
        $this->modele=new ModeleGallery();
    }

    public function gallery(){

        $photos=$this->modele->getPhotos();
        $video=$this->modele->getVideo();
        $photoPostPhoto=$this->modele->photoInPost();
        $postTag=$this->modele->postTag();
        $tags=$this->modele->getTag();
        $this->vue->vue_Gallery($photos,$video,$photoPostPhoto,$postTag,$tags);
    }

    public function gallery_err($err){
        $token= $this->modele->createToken();
        $this->vue->vue_err_Gallery($token,$err);
    }

    public function formulaire_ajout_gallery(){
        $token= $this->modele->createToken();
        $this->vue->vue_ajouter_gallery($token);
    }

    public function formulaire_ajout_video_gallery(){
        $token= $this->modele->createToken();
        $this->vue->vue_ajouter_video_gallery($token);
    }

    public function modif_video($tab){
        $id=$tab['id_video'];
        $titre=$tab['titre_video'];
        $description=$tab['description_video'];
        $this->modele->modif_video($id,$titre,$description);
        header("Location: ?module=admin_video&form=modifier");
    }

    public function modif_photo($tab){

        $id=$tab['id_photo'];
        $titre=$tab['titre_photo'];
        $description=$tab['description_photo'];
        $this->modele->modif_photo($id,$titre,$description);
        header("Location: ?module=admin_photo&form=modifier");
    }
    public function ajout_video($tab){

        $url=$tab['video'];
        $titre=$tab['titre_video'];
        $description=$tab['description'];
        $this->modele->ajouter_video($url,$titre,$description);
        header("Location: ?module=admin_video&form=ajouter");
    }

    public function ajout_photo($tab){
        $titre=$tab['titre_photo'];
        $description=$tab['description'];

        $target_dir = "image/";
        $target_file = $target_dir . basename(time()."".$_FILES["photo"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["photo"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                $uploadOk = 0;
            }
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {

               $this->modele->ajouter_photo($target_file,$titre,$description) ;
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
 header("Location: ?module=admin_photo&form=ajouter");
    }


    public function photo_admin(){
        $photos=$this->modele->getPhotos();
        $photoPostPhoto=$this->modele->photoInPost();
        $postTag=$this->modele->postTag();
        $tags=$this->modele->getTag();
        $token=$this->modele->createToken();
        $this->vue->vue_photo_admin($photos,$photoPostPhoto,$postTag,$tags,$token);
    }
    
    public function video_admin(){
        $token=$this->modele->createToken();
        $video=$this->modele->getVideo();
        $this->vue->vue_video_admin($video,$token);
    }

}
