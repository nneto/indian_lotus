<?php

include_once('controleur_gallery.php');
class ModGallery extends ModuleGenerique
{
    public function module_gallery(){
        $this->controleur= new ControleurGallery();
        $this->controleur->gallery();
    }

    public function module_formulaire_ajout_gallery(){
        if($this->isAdmin()){
            $this->controleur= new ControleurGallery();
            $this->controleur->formulaire_ajout_gallery();    
        }
        
    }

    public function module_formulaire_ajout_video_gallery(){
        if($this->isAdmin()){
            $this->controleur= new ControleurGallery();
            $this->controleur->formulaire_ajout_video_gallery();    
        }
        
    }
    public function module_ajout_gallery($tab){
        if($this->isAdmin()){
            if(isset($_FILES['photo']) && isset($tab['titre_photo']) && isset($tab['description'])){
            $this->controleur= new ControleurGallery();
            $this->controleur->ajout_photo($tab);
        }else{
        }    
        }
    }

    public function module_ajout_video_gallery($tab){
        if($this->isAdmin()){
            if(isset($tab['video']) && isset($tab['titre_video']) && isset($tab['description'])){
                $this->controleur= new ControleurGallery();
                $this->controleur->ajout_video($tab);
            }else{

            }    
        }
    }
    
    public function module_modif_gallery($tab){
        
        if($this->isAdmin()){
            if(isset($tab['id_photo'])&& isset($tab['titre_photo']) && isset($tab['description_photo'])){              
                $this->controleur= new ControleurGallery();
                $this->controleur->modif_photo($tab);
            }else{
              
            }    
        }
    }

    public function module_modif_video_gallery($tab){
        if($this->isAdmin()){
            if(isset($tab['id_video'])&& isset($tab['titre_video']) && isset($tab['description_video'])){
                $this->controleur= new ControleurGallery();
                $this->controleur->modif_video($tab);
            }else{
            }    
        }
    }
    
    public function module_err_accueil($err){
        $this->controleur= new ControleurGallery();
        $this->controleur->gallery_err($err);
    }
    
    public function photo_admin(){
        if($this->isAdmin()){
            $this->controleur= new ControleurGallery();
            $this->controleur->photo_admin();        
        }
    }
    
    public function video_admin(){
        if($this->isAdmin()){
            $this->controleur= new ControleurGallery();
            $this->controleur->video_admin();    
        }
    }
}
